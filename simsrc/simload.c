#include <stdio.h>
#include <unistd.h>
#include <fcntl.h>
#include <string.h>

#include "simdef.h"

#ifdef HOST
#include "mdexlib.h"
#endif

/*
 * MDEX executable header
 */
static struct mdexhdr {
	short mword;		/* 'magic word'	0xffff	*/
	short dummy[2];		/* dummy words		*/
	short ldaddr;		/* load address		*/
	short imglen;		/* image length		*/
	short iniWP;		/* initial WP value	*/
	short iniPC;		/* initial PC value	*/
	short filler[57];	/* fill to 128 bytes	*/
} hdr;

#define O_BINARY 0

/*
 * Read a.out header. Return 0 on error.
 */
int
readhdr(fd, hdr)
	int fd;
	register struct mdexhdr *hdr;
{
	unsigned char buf[128];

	if (read(fd, buf, 128) != 128)
		return 0;
	hdr->mword =	buf[0]  << 8 | buf[1];
	hdr->ldaddr =	buf[6]  << 8 | buf[7];
	hdr->imglen =	buf[8]  << 8 | buf[9];
	hdr->iniWP =	buf[10] << 8 | buf[11];
	hdr->iniPC =	buf[12] << 8 | buf[13];
	return 1;
}

unsigned short loadaddr;
extern uint16 pcreg;		/* The program PC */
extern uint16 wpreg;		/* The program Workspace Pointer */
extern uint32 mpcreg;		/* Mapped PC */
uint16 sysbrk;			/* The "break" */

int check_mdex(char *file)
{
	int fd, loadaddr;

	fd = open(file, O_RDONLY|O_BINARY);
	if (fd<0)
		return 0;

	readhdr(fd, &hdr);
	loadaddr = (unsigned short)hdr.ldaddr;
	/* only MDEX loads above 0xB800 */
	return (loadaddr > 0xB800);
}

extern char   *path;    /* Given path to executable */

/* Add path to command name */
char *add_path(char *command)
{
	static char cmd_line[256];
	
	strcpy(cmd_line, path);
	if (command[0]=='1' && (command[1]==':' || command[1]=='/'))
		command[1] = '/';
	else
		strcat(cmd_line, "1/");
	strcat(cmd_line, command);
	return cmd_line;
}

int file_ld(char *file)
{
	int fd, init_len, brk;

	fd = open(file, O_RDONLY|O_BINARY);
	if (fd<0) {
		file = add_path(file);
		fd = open(file, O_RDONLY|O_BINARY);
	}
	if (fd<0)
		return -1;

	readhdr(fd, &hdr);
	//printf("magic=%x, PC=%x, WP=%x\n", hdr.mword, hdr.iniPC, hdr.iniWP);
	loadaddr = (unsigned short)hdr.ldaddr;
	init_len = (unsigned short)hdr.imglen;
	read(fd, memory+loadaddr, init_len);
	close(fd);
	
	mpcreg = pcreg = (unsigned)hdr.iniPC;
	wpreg = (unsigned)hdr.iniWP;
	wpreg = wpreg ? wpreg : 0xf000;
	sysbrk = hdr.ldaddr + hdr.imglen;

	return 0;
}

int binloader(char *file, int loadpt)
{

	int rc;

#ifdef HOST
	/* load MDEX as a library & prep vector */
	for(unsigned i=0xc000; i<0xc000+mdexlib_len; i++) {
		memory[i] = mdexlib[i+0x80-0xc000];
	}
	for(unsigned i=0xc000; i<0xc100; i++) {
		memory[i-0xc000] = memory[i];
	}
#endif

	/* load target binary to run */
	rc = file_ld(file);
	if (rc<0)
		return -1;

	return 0;
}

