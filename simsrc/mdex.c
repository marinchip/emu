/* Emulate MDEX system calls
 * Based on ideas of "apout" written by Warren Toomey
 */
 
#include <string.h>
#include <stdlib.h>
#include <unistd.h>
#include <fcntl.h>
#include <ctype.h>

#include "simdef.h"

extern uint16 statreg;	/* The program status register */
extern uint16 pcreg;	/* The program counter register */
extern uint32 mpcreg;	/* Mapped PC */

void mdex_init()
{
	return;
}

extern int run;
extern uint16 sysbrk;			/* The "break" */
extern int ptraceenable;
extern char   *path;    /* Given path to executable */
int param_flag;

char *add_path(char *command);
int  file_ld(char *file);
void mkrare();
void treset();
int  israw;

void mdex_call(int op, int wp, int pc, int st)
{
	int i, fd;
	char nm[256];
	
	uint8_t *msg = memory + op;
	// printf("JSYS %x, wp=%04x, pc=%04x, st=%04x\n", (char*)msg[0], wp, pc, st);
	switch (msg[0]) {

	case 2: /* exit */ {
		int   sts = (int)(msg[2]<<8 | msg[3]);
		if( ptraceenable) printf("EXIT %d\n", sts);
		run = 0;
		return;
	}
	
	case 7: /* create */
	case 5: /* open */ {
		int   m, len  = msg[2];
		char *name = (char*)(memory + (msg[4]<<8 | msg[5]));
		int   mode = (int)(msg[6]<<8 | msg[7]);
		int   priv = (int)(msg[8]<<8 | msg[9]);
		
		int   tran[3] = {O_RDWR, O_WRONLY, O_RDONLY};
		while ( *name && *name==' ') { name++; len--; } /* skip leading spaces */
		for(int i=0; i<len; i++) {
			nm[i] = tolower(name[i]);
			if( nm[i]==' ' ) nm[i] = 0;
			if( nm[i]==0) break;
		}
		nm[len] = 0;
		if( ptraceenable) printf("%s %s, %d, %x => ", (*msg==7)?"CREATE":"OPEN", nm, mode, priv);

		if( !strcmp(nm, "param.dev") ) {
			msg[1] = 0;
			msg[3] = 255;
			if( ptraceenable) printf("255\n");
			return;
		}
		if( !strcmp(nm, "cons.dev") ) {
			msg[1] = 0;
			msg[3] = 0;
			if( ptraceenable) printf("0\n");
			return;
		}
		/* map files starting with 1/ or 2/ to the path directory */
		if ((nm[1]=='/' || nm[1]==':') && (nm[0]=='1' || nm[0]=='2')) {
			char buf[256];
			nm[1] = '/';
			strcpy(nm, add_path(nm));
		}
		mode = tran[mode & 0x3];
		if(msg[0]==7) mode |= O_CREAT;
		if(priv==0) priv = 0x1ff;
		fd = open(nm, mode, priv);
		if(fd < 0) {
			msg[1] = 3;
			msg[3] = 0;
			if( ptraceenable) printf("<err>\n");
			return;
		}
		else {
			msg[1] = 0;
			msg[3] = fd;
			if( ptraceenable) printf("%d\n", fd);
			return;
		}
		exit(1);
		}
	
	case 6: /* close */ {
		int   idx  = msg[3];
		if( ptraceenable) printf("CLOSE %d\n", idx);

		msg[1] = 0;
		if( idx==0 || idx==255 ) {
			return;
		}
		close(idx);
		return;
		}

	case 8: /* NOS only: link */ {
		int   m, len1  = msg[2], len2 = msg[6];
		char  nm2[256];
		char *newnm = (char*)(memory + (msg[4]<<8 | msg[5]));
		char *oldnm = (char*)(memory + (msg[8]<<8 | msg[9]));
		
		while ( *oldnm && *oldnm==' ') { oldnm++; len2--; } /* skip leading spaces */
		for(int i=0; i<len2; i++) {
			nm[i] = tolower(oldnm[i]);
			if( nm[i]==' ' ) nm[i] = 0;
			if( nm[i]==0) break;
		}
		nm[len2] = 0;
		while ( *newnm && *newnm==' ') { newnm++; len1--; } /* skip leading spaces */
		for(int i=0; i<len1; i++) {
			nm2[i] = tolower(newnm[i]);
			if( nm2[i]==' ' ) nm2[i] = 0;
			if( nm2[i]==0) break;
		}
		nm2[len1] = 0;
		
		if( ptraceenable) printf("LINK old %s -> new %s\n", nm, nm2);
		fd = link(nm, nm2);
		if(fd < 0) {
			msg[1] = 3;
			return;
		}
		else {
			msg[1] = 0;
			return;
		}
		exit(1);
		}
		
	case 9: /* delete */ {
		int   m, len  = msg[2];
		char *name = (char*)(memory + (msg[4]<<8 | msg[5]));
		
		while ( *name && *name==' ') { name++; len--; } /* skip leading spaces */
		for(int i=0; i<len; i++) {
			nm[i] = tolower(name[i]);
			if( nm[i]==' ' ) nm[i] = 0;
			if( nm[i]==0) break;
		}
		nm[len] = 0;
		if( 1|| ptraceenable) printf("DELETE %s\n", nm);
	
		if( !strcmp(nm, "param.dev") ) {
			msg[1] = 0;
			return;
		}
		if( !strcmp(nm, "cons.dev") ) {
			msg[1] = 0;
			return;
		}
		/* map files starting with 1/ or 2/ to the path directory */
		if ((nm[1]=='/' || nm[1]==':') && (nm[0]=='1' || nm[0]=='2')) {
			char buf[256];
			nm[1] = '/';
			strcpy(nm, add_path(nm));
		}
		fd = unlink(nm);
		if(fd < 0) {
			msg[1] = 3;
			return;
		}
		else {
			msg[1] = 0;
			return;
		}
		exit(1);
		}

	case 11: /* read */ {
		int   idx  = msg[3];
		char *buf = (char*)(memory + (msg[4]<<8 | msg[5]));
		int   len = (int)(msg[6]<<8 | msg[7]);
		if( ptraceenable) printf("READ %d, %lx, %d\n", idx, buf-(char*)memory, len);

		/* Read from the parameter string */
		if(idx==255) {
			char *p = (char*)(memory+0xf800);
			int l = strlen(p) + 1;

			if (l < len) len = l;
			strncpy(buf, p, len);
			msg[8] = len>>8; msg[9] = len & 0xff;
			msg[1] = 0;
			param_flag = 1;
			return;
		}

		/* When reading from console, assume text and break at each line.
		 * This is needed for script 'here' files, which would otherwise
		 * arrive with lines concatenated.
		 */
		if( idx==0) {
			char c;
			int i;
			if( israw ) {
				while( read(idx, &c, 1)<1 ) usleep(100000);
				if( c=='\n' ) c = '\r';
				buf[0] = c;
				msg[8] = 0; msg[9] = 1;
				msg[1] = 0;
				return;
			}
			for(i=0; i<len; i++) {
				fd = read(idx, &c, 1);
				if( fd<1 ) break;
				if(c=='\n') c='\r';
				buf[i] = c;
				if(c=='\r') break;
			}
			if( i<len ) len = i+1;
			msg[8] = len>>8; msg[9] = len & 0xff;
			msg[1] = len ? 0 : 1;
			return;
		}
		
		/* Read from regular file */
		fd = read(idx, buf, len);
		if(fd < 0) {
			msg[1] = 2;
			return;
		}
		else {
			msg[8] = fd>>8; msg[9] = fd;
			msg[1] = fd ? 0 : 1;
			return;
		}
		exit(1);
		}
		
	case 12: /* write */ {
		int   idx  = msg[3];
		char *buf = (char*)(memory + (msg[4]<<8 | msg[5]));
		int   len = (int)(msg[6]<<8 | msg[7]);
		if( ptraceenable) printf("WRITE %d, %d\n", idx, len);
		
		/* Not allowed to write to param dev */
		if( idx==255 ) {
			msg[1] = 1;
		}
		
		/* Console terminal write goes to stdout */
		if( idx==0 ) {
			for(int i=0; i<len; i++) {
				if(buf[i]==13) buf[i] = 10;
				write(1,buf+i,1);
			}
			msg[8] = msg[6]; msg[9] = msg[7];
			msg[1] = 0;
			return;
		}
		
		/* Write to regular file */
		fd = write(idx, buf, len);
		if(fd < 0) {
			msg[1] = 2;
			return;
		}
		else {
			msg[8] = fd>>8; msg[9] = fd;
			msg[1] = 0;
			return;
		}
		exit(1);
		}

	case 13: /* seek */ {
		int ofs = (int)(msg[4]<<24 | msg[5]<<16 | msg[6]<<8 | msg[7]);
		int whence = msg[2];
		int idx = msg[3];
		if( ptraceenable) printf("SEEK %d ofs=%x base=%d\n", idx, ofs, whence);
		
		ofs = lseek(idx, ofs, whence);
		msg[8] = ofs>>24; msg[9] = ofs>>16; msg[10] = ofs>>8; msg[11] = ofs;
		msg[1] = 0;
		return;
		}

	case 14: /* trap */ {
		if( ptraceenable) printf("TRAP\n");
		msg[1] = 0;
		return;
		}

	case 15: /* mem */ {
		int len, idx = (int)(msg[2]<<8 | msg[3]);
		if( ptraceenable) printf("MEM %d\n", idx);
		
		if( idx!=1 ) {
			msg[1] = 6;
			return;
		}
		msg[4] = sysbrk>>8; msg[5] = sysbrk & 0xff;
		len = 0xc000 - sysbrk;
		msg[6] = len>>8; msg[7] = len & 0xff;
		msg[1] = 0;
		return;
		}
		
	case 16: /* ioctl */ {
		int idx = msg[3];
		char *buf = (char*)(memory + (msg[4]<<8 | msg[5]));
		if( ptraceenable) printf("IOCTL\n");
		
		if (idx==0 && buf[0]==1) {
			if (buf[1]==1) {
				israw = 1;
				mkrare();
			} else {
				israw = 0;
				treset();
			}
		}
		msg[1] = 0;
		return;
		}

	case 17: /* exec */ {
		int   j, len  = msg[2];
		char *name = (char*)(memory + (msg[4]<<8 | msg[5]));
		char cmd_line[81];
		
		len = (len > 80) ? 80 : len;
		memcpy(cmd_line, name, len);
		cmd_line[len] = 0;
		if( ptraceenable) printf("EXEC (%d) = '%s'\n", len, cmd_line);
		
		/* find and copy params */
		for(i=0; i<len; i++) if (cmd_line[i]==' ') break;
		for(j=i; j<len; j++) {
			memory[0xf800+j-i] = cmd_line[j];
		}
		memory[0xf800+j] = 0;

		/* open new exe */
		cmd_line[i] = 0;
		if (file_ld(cmd_line)<0) {
			msg[1] = 3;
			return;
		}
		
		/* reset ioctl to normal cooked mode */
		treset();
		return;
		}

	case 36: /* NOS only: flock */ {
		/* ignore lock & unlock on MDEX */
		msg[1] = 0;
		return;
		}
	default:
		printf("JSYS %x, wp=%04x, pc=%04x, st=%04x\n", msg[0], wp, pc, st);
		exit(1);
	}
}
