
INTRODUCTION

In the late 1970's the company Marinchip sold S-100 bus based systems around
the Texas Instruments 9900 CPU chip. This was a 16-bit CPU released in 1976
with an instruction set architecture reminiscent of the PDP-11. Marinchip
combined the hardware with a nice software stack, inclusing a Unix-like
operating system. This software was later ported to the "Powertran Cortex"
computer system, which was developed by TI in the UK.

See the below two webpages for more detail:

-  https://www.fourmilab.ch/documents/marinchip/   and
-  http://www.powertrancortex.com

This repo contains an emulator for a Marinchip-like system. It is modeled on the
mini-Cortex hardware (see: http://www.stuartconner.me.uk/mini_cortex/mini_cortex.htm),
but with a disk hardware variation as discussed below.

It builds in two versions:

- One version, called "host", emulates the hardware but intercepts all (MDEX)
system calls and reroutes them to the host environment. This allows running
the Marinchip software in the host file system and is used to cross build the
various packages from source. The key parts of the Marinchip tool chain are
provided in the directory "1" for this purpose.

- The other version, called "sim", emulates all the hardware and is used to
run the MDEX and NOS/MT operating systems, working with disk images. The disk
hardware is not the mini-Cortex CF Card, but a hypothetical IDE device with
256 byte sectors. This is done to have a better match with historical hardware.

The CPU part of the emulators is derived from Dave Pitts' excellent TI990
emulator.

Note that the emulator has been tested on MacOS and may need some tweaking on
other systems.


USING HOST MODE

Running a Marinchip program in host mode is done with:

  ./host [-t] [-p] <program> <params>
  
The '-t' option enables tracing all instructions executed. The '-p' option
enables tracing all (intercepted) system calls. As an example, the following
runs the assembler:

  ./host asm test.rel=test.asm
  
Note that files must be in Marinchip format (i.e. text lines are separated by
CR not NL, and all text files end with EOT; use spaces instead of tabs).


USING WITH MDEX

First unzip the MDEX image. This results in three files: the MDEX OS binary
and two disk images ('mdex.sav', 'mdex1.dsk' and 'mdex2.dsk' respectively).
Run with the command:

  ./sim mdex.sav

The first disk (called '1/' inside MDEX) contains the standard distribution,
the second disk (called '2/' inside MDEX) contains a variety of games.

If you want to use other disk images (for example from the Powertran Cortex
website), specify the name of the image on the command line:

  ./sim mdex.sav mdex1.dsk <other disk img>

Instead of re-specifying mdex1.dsk, it is also possible to use a dot, which
means "use default name":

  ./sim mdex.sav . <other disk img>


USING WITH NOS/MT
  
First unzip the NOSMT image. This results in four files: the NOSMT OS binary
and three disk images: 'noshd.dsk', 'nosfd.dsk' and 'newhd.dsk'. The first
is a 10MB fully installed harddisk, including all surviving source code. The
second is a 640KB floppy image containing "MPE forth". The last is an empty
10 MB harddisk which can be used for backup or experimentation. They mount
as '1:', '2:' and '3:' respectively. Only the first one is auto-mounted.

Run with the command:

  ./sim nos.sav
  
Log in as "low" with password "down". Other disk images can be specified in
the same manner as described for MDEX above.

