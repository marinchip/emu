/***********************************************************************
*
* simops.c - Simulate instruction operations for the TI 990 computer.
*
* Changes:
*   05/29/03   DGP   Original.
*   06/22/03   DGP   Fixed XOP REG function.
*   07/08/03   DGP   Fixed status on Subtract and indexing with negative
*                    values.
*   07/09/03   DGP   Forced PC and WP to be on even boundry.
*   07/13/03   DGP   Forced word and address access to even boundry.
*   08/07/03   DGP   Fixed carry on Add and Add Byte.
*                    Fixed MPY and DIV (had signed problems).
*                    Fixed B Rn (branch to register).
*   08/12/03   DGP   Added Map file instructions and priv checks.
*   11/26/03   DGP   Fixup OVER & CARRY status checking.
*   12/10/03   DGP   Streamline TILINE & ROM access with 20 bits.
*   01/26/04   DGP   Added memory size option.
*   04/20/04   DGP   Changed SLA to detect sign change during shift.
*   05/06/04   DGP   Fix LREX to run in 990/10 mode.
*   06/14/04   DGP   Added floating point instructions.
*   06/16/04   DGP   Added extended /12 instructions.
*   07/13/04   DGP   Added /12 memory protection checks
*   03/30/05   DGP   Fixed XOP for Mapping.
*   01/09/07   DGP   Added EMD code.
*
***********************************************************************/

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <memory.h>
#include <ctype.h>

#include "simdef.h"

extern uint16 pcreg;	/* The program PC */
extern uint16 opcreg;	/* The old program PC */
extern uint16 statreg;	/* The program status register */
extern uint16 wpreg;	/* The program Workspace Pointer */
extern uint16 lights;	/* The panel lights */
extern uint32 mpcreg;	/* Mapped PC */
extern uint32 breakaddr;/* The break point address */
extern long pancount;	/* Instructions executed for panel */
extern unsigned long instcount;

extern int run;
extern int idle;
extern int runled;
extern int idleled;
extern int model;
extern int breakflag;
extern int enablepanel;
extern int mapenabled;
extern int intsenabled;
extern int traceenable;
extern int ptraceenable;
extern int tracestart;
extern int traceend;
extern int tracelim[2];
extern int deferint;
extern int traceit;
extern int windowlen;
extern uint16 mapcrudata;
extern uint16 curmapindex;
extern uint16 errcrudata;
extern uint16 curinst;
extern uint32 memlen;
extern uint32 maplatch;
extern long delayclock;
extern char view[MAXVIEW][81];
extern FILE *tracefd;
extern uint32 tracemem[16];
extern uint32 errtrace[16];
extern int errindex;
extern int trcindex;

extern uint8 memory[MEMSIZE];
extern MapFile mapfile[MAPSIZE];

static void runinst (uint16);
static int use_LDD_map = FALSE;
static int use_LDS_map = FALSE;
static int use_map_cnt = 0;
static int tiline = FALSE;
static int inhibitwrite = FALSE;

static int proc1op (uint16 inst);
static int proc2op (uint16 inst);
static int proc3op (uint16 inst);
static int proc4op (uint16 inst);
static int proc5op (uint16 inst);
static int proc6op (uint16 inst);
static int proc7op (uint16 inst);
static int proc8op (uint16 inst);
static int proc9op (uint16 inst);
static int proc10op (uint16 inst);

// '612 mapper chip
uint32 mapper[16];
uint32 flag;

// trace buffer
#define TRACSIZ 4000
char lines[TRACSIZ][256];
char *lp;
int ii;

typedef struct {
	int (*proc) (uint16 inst);
} Inst_Proc;

Inst_Proc inst_proc[MAX_INST_TYPES] = {
	proc1op,  proc2op,  proc3op,  proc4op,  proc5op,  proc6op,  proc7op,
	proc8op,  proc9op,  proc10op
};

/***********************************************************************
* decodeinst - Decode instructions.
***********************************************************************/

static int decodeinst (uint16 inst)
{
	if (inst & 0xF000)
	{
		if (inst & 0xC000) return TYPE_1;
		if ((inst & 0xF800) == 0x3000) return TYPE_4;
		if ((inst & 0xF000) == 0x3000) return TYPE_9;
		if ((inst & 0xF000) == 0x2000) return TYPE_3;
		return TYPE_2;
	}
	else if (inst & 0x0F00)
	{
		if ((inst & 0x0F00) == 0x0F00) return TYPE_6;
		else if ((inst & 0x0F00) == 0xE00)
		{
			if ((inst & 0x0FF0) == 0x0E00) return TYPE_15;
			if ((inst & 0x0FC0) == 0x0E00) return TYPE_12;
			return TYPE_6;
		}
		else if ((inst & 0x0F00) == 0x0D00) return TYPE_6;
		else if ((inst & 0x0F00) == 0x0C00)
		{
			if ((inst & 0x0FFF) == 0x0C0F) return TYPE_7;
			if ((inst & 0x0FFF) == 0x0C0E) return TYPE_7;
			if ((inst & 0x0FFF) == 0x0C08) return TYPE_11;
			if ((inst & 0x0FFE) == 0x0C0C) return TYPE_17;
			if ((inst & 0x0FF8) == 0x0C08) return TYPE_14;
			if ((inst & 0x0FF0) == 0x0C00) return TYPE_7;
			if ((inst & 0x0FC0) == 0x0C00) return TYPE_16;
			return TYPE_6;
		}
		else if ((inst & 0x0C00) == 0x0800) return TYPE_5;
		else if ((inst & 0x0C00) == 0x0400) return TYPE_6;
		else if ((inst & 0x0F00) == 0x0300)
		{
			if (inst == 0x0300) return TYPE_8;
			if ((inst & 0x0FF0) == 0x03F0) return TYPE_21;
			if ((inst & 0x0FE0) == 0x0320) return TYPE_10;
			return TYPE_7;
		}
		else if ((inst & 0x0F00) == 0x0200) return TYPE_8;
		else if ((inst & 0x0F00) == 0x0100) return TYPE_6;
	}
	else if (inst & 0x00F0)
	{
		if ((inst & 0x00F0) == 0x00B0) return TYPE_8;
		else if ((inst & 0x00F0) == 0x0070) return TYPE_18;
		else if ((inst & 0x00F0) == 0x0080) return TYPE_8;
		else if ((inst & 0x00F0) == 0x0090) return TYPE_8;
		else if ((inst & 0x0040) == 0x0040) return TYPE_12;
		else if ((inst & 0x00F0) == 0x0020)
		{
			if ((inst & 0x00FF) == 0x0021) return TYPE_20;
			if ((inst & 0x00FF) == 0x0022) return TYPE_20;
			if ((inst & 0x00FF) == 0x002B) return TYPE_19;
			if ((inst & 0x00FC) == 0x002C) return TYPE_7;
			if ((inst & 0x00FC) == 0x0028) return TYPE_11;
			return TYPE_11;
		}
		else if ((inst & 0x00F0) == 0x0010)
		{
			if ((inst & 0x00FE) == 0x001E) return TYPE_11;
			return TYPE_13;
		}
		return TYPE_18;
	}
	return (TYPE_ILL);
}


/***********************************************************************
* tracememory - Trace memory data
***********************************************************************/

void tracememory (uint32 code, uint32 ma)
{
	if (model < 12) return;
	
	errtrace[errindex] = code | (ma & 0x1FFFFE);
	errindex++;
	if (errindex == 16) errindex = 0;
}

/***********************************************************************
* mapaddr - Map address to a 22 bit physical address.
***********************************************************************/

static int remap[16] = {
	0x0000, 0x2000, 0x4000, 0x6000, 0x8000, 0xa000, 0xc000, 0xe000,
	0x1000, 0x3000, 0x5000, 0x7000, 0x9000, 0xb000, 0xd000, 0xf000
};

uint32 mapaddr (uint16 pa)
{
	uint32 ma, page, map, offs;

	if (mapenabled) {
		page = (pa>>12) & 0xf;
		map = mapper[page];
		if((map>>12)<16) {
			map = remap[map>>12];
		}
		offs = pa & 0x0fff;
		map &= 0x7fffff;
		ma = map + offs;
//if(ma<0x0080) {printf("loc <0x80, pc=%x, map[%d]=%x\n", pcreg, 5, mapper[5]);}
	} else {
		ma = pa;
	}
	return ma;
}

/***********************************************************************
* getinst - Get an Instruction.
***********************************************************************/

uint16 getinst ()
{
	uint32 ma;

	mpcreg = pcreg;
	ma = pcreg;
	ma = mapaddr(pcreg);
	return (GETMEM0 (ma));
}

/***********************************************************************
* getreg - Get a register value.
***********************************************************************/

uint16 getreg (uint16 r)
{
	uint32 ma;
	
	ma = wpreg + (r * 2);
	ma = mapaddr(ma);
	return (GETMEM0 (ma));
}

/***********************************************************************
* putreg - Put a value into a register.
***********************************************************************/

void putreg (uint16 r, uint16 v)
{
	uint32 ma;
	
	ma = wpreg + (r * 2);
	ma = mapaddr(ma);
	if (!inhibitwrite)
	{
		PUTMEM0 (ma, v);
	}
}

/***********************************************************************
* getmem - Get a word from memory.
***********************************************************************/

uint16 getmem (uint16 pa, int srcdst)
{
	uint32 ma;
	
	ma = pa & 0xFFFE;
	ma = mapaddr(ma);
	return (GETMEM0 (ma));
}

/***********************************************************************
* getmemb - Get a byte from memory.
***********************************************************************/

uint8 getmemb (uint16 pa, int srcdst)
{
	uint32 ma;
	
	ma = pa;
	ma = mapaddr(ma);
	return (GETMEMB0 (ma));
}

/***********************************************************************
* putmem - Put a word in memory.
***********************************************************************/

void putmem (uint16 pa, uint16 v, int srcdst)
{
	uint32 ma;

	if (pa < memlen)
	{
		ma = pa & 0xFFFE;
		ma = mapaddr(ma);

//if ((ma>=0x0000 && ma<0x0080)||(ma>=0x0090 && ma<0x1000)) {
//printf("PC=%x, ma=%x, val=%x\n", mapaddr(pcreg), ma, v);
//}
		PUTMEM0 (ma, v);
	}
}

/***********************************************************************
* putmemb - Put a byte in memory.
***********************************************************************/

void putmemb (uint16 pa, uint8 v, int srcdst)
{
	uint32 ma;
	
	ma = pa;
	ma = mapaddr(ma);
//if ((ma>=0x0000 && ma<0x0080)||(ma>=0x0090 && ma<0x1000)) {
//printf("PC=%x, ma=%x, val=%x\n", mapaddr(pcreg), ma, v);
//}
	if (ma < memlen)
	{
		PUTMEMB0 (ma, v);
	}
}

/***********************************************************************
* getaddr - Get an effective address.
***********************************************************************/

uint16 getaddr (uint16 sr, uint16 st, int inc)
{
	uint16 sa = 0;
	
	switch (st)
	{
	case 0:
		
		if (traceit)
			lp += sprintf (lp, " R%d", sr);
		sa = wpreg + (sr * 2);
		break;
		
	case 1:
		
		if (traceit)
			lp += sprintf (lp, " *R%d", sr);
		sa = GETREG (sr);
		break;
		
	case 2:
		
		sa = GETINST;
		if (traceit)
		{
			if (sr == 0)
				lp += sprintf (lp, " @>%04X", sa);
			else
				lp += sprintf (lp, " @>%04X(%d)", sa, sr);
		}
		if (inc)
		{
			pcreg += 2;
			mpcreg += 2;
		}
		if (sr != 0) sa = (int16)sa + (int16)GETREG (sr);
		break;
		
	case 3:
		
		if (traceit)
			lp += sprintf (lp, " *R%d+", sr);
		sa = GETREG (sr);
		if (inc) PUTREG (sr, sa + 2);
		break;
	}
	if (traceit)
		lp += sprintf (lp, " sa=>%04X", sa);

	return (sa);
}

/***********************************************************************
* getbyte - Get a byte operand. If memory access is past the end of
* defined memory, return -1;
***********************************************************************/

uint8 getbyte (uint16 sr, uint16 st, int inc, int srcdst)
{
	uint16 sa;
	uint8  sval = 0xFF;
	
	switch (st)
	{
	case 0:
		
		if (traceit && inc)
			lp += sprintf (lp, " R%d", sr);
		sval = (GETREG (sr) & 0xFF00) >> 8;
		break;
		
	case 1:
		
		if (traceit && inc)
			lp += sprintf (lp, " *R%d", sr);
		sa = GETREG (sr);
		sval = GETMEMB (sa, srcdst);
		break;
		
	case 2:
		
		sa = GETINST;
		if (traceit && inc)
		{
			if (sr == 0)
				lp += sprintf (lp, " @>%04X", sa);
			else
				lp += sprintf (lp, " @>%04X(%d)", sa, sr);
		}
		if (inc)
		{
			pcreg += 2;
			mpcreg += 2;
		}
		if (sr != 0) sa = (int16)sa + (int16)GETREG (sr);
		sval = GETMEMB (sa, srcdst);
		break;
		
	case 3:
		
		if (traceit && inc)
			lp += sprintf (lp, " *R%d+", sr);
		sa = GETREG (sr);
		sval = GETMEMB (sa, srcdst);
		if (inc) PUTREG (sr, sa + 1);
		break;
	}
	return (sval);
}

/***********************************************************************
* getword - Get a word operand. If memory access is past the end of
* defined memory, return -1;
***********************************************************************/

uint16 getword (uint16 sr, uint16 st, int inc, int srcdst)
{
	uint16 sa;
	uint16 sval = 0xFFFF;
	
	switch (st)
	{
	case 0:
		
		if (traceit && inc)
			lp += sprintf (lp, " R%d", sr);
		sval = GETREG (sr);
		break;
		
	case 1:
		
		if (traceit && inc)
			lp += sprintf (lp, " *R%d", sr);
		sa = GETREG (sr);
		sval = GETMEM (sa & 0xFFFE, srcdst);
		break;
		
	case 2:
		
		sa = GETINST;
		if (traceit && inc)
		{
			if (sr == 0)
				lp += sprintf (lp, " @>%04X", sa);
			else
				lp += sprintf (lp, " @>%04X(R%d)", sa, sr);
		}
		if (inc)
		{
			pcreg += 2;
			mpcreg += 2;
		}
		if (sr != 0) sa = (int16)sa + (int16)GETREG (sr);
		sval = GETMEM (sa & 0xFFFE, srcdst);
		break;
		
	case 3:
		
		if (traceit && inc)
			lp += sprintf (lp, " *R%d+", sr);
		sa = GETREG (sr);
		sval = GETMEM (sa & 0xFFFE, srcdst);
		if (inc) PUTREG (sr, sa + 2);
		break;
	}
	return (sval);
}

/***********************************************************************
* putaddr - Bump address by count
***********************************************************************/

void putaddr (uint16 sr, uint16 st, uint16 inc)
{
	if (st == 3)
	{
		PUTREG (sr, GETREG (sr) + inc);
	}
}

/***********************************************************************
* putbyte - Put a byte operand.  If memory access is beyond allowed 
* memory (in ROM space) it is ignored.
***********************************************************************/

static void putbyte (uint16 dr, uint16 dt, uint8 dval, int srcdst)
{
	uint16 da;
	
	switch (dt)
	{
	case 0:
		
		if (traceit)
			lp += sprintf (lp, " R%d", dr);
		da = (GETREG (dr) & 0x00FF) | (dval << 8);
		PUTREG (dr, da);
		break;
		
	case 1:
		
		if (traceit)
			lp += sprintf (lp, " *R%d", dr);
		da = GETREG (dr);
		PUTMEMB (da, dval, srcdst);
		break;
		
	case 2:
		
		da = GETINST;
		if (traceit)
		{
			if (dr == 0)
				lp += sprintf (lp, " @>%04X", da);
			else
				lp += sprintf (lp, " @>%04X(%d)", da, dr);
		}
		pcreg += 2;
		mpcreg += 2;
		if (dr != 0) da = (int16)da + (int16)GETREG (dr);
		PUTMEMB (da, dval, srcdst);
		break;
		
	case 3:
		
		if (traceit)
			lp += sprintf (lp, " *R%d+", dr);
		da = GETREG (dr);
		PUTREG (dr, da + 1);
		PUTMEMB (da, dval, srcdst);
		break;
	}
}

/***********************************************************************
* putword - Put a word operand.  If memory access is beyond allowed 
* memory (in ROM space) it is ignored.
***********************************************************************/

void putword (uint16 dr, uint16 dt, uint16 dval, int srcdst)
{
	uint16 da;
	
	switch (dt)
	{
	case 0:
		
		if (traceit)
			lp += sprintf (lp, " R%d", dr);
		PUTREG (dr, dval);
		break;
		
	case 1:
		
		if (traceit)
			lp += sprintf (lp, " *R%d", dr);
		da = GETREG (dr);
		PUTMEM (da, dval, srcdst);
		break;
		
	case 2:
		
		da = GETINST;
		if (traceit)
		{
			if (dr == 0)
				lp += sprintf (lp, " @>%04X", da);
			else
				lp += sprintf (lp, " @>%04X(%d)", da, dr);
		}
		pcreg += 2;
		mpcreg += 2;
		if (dr != 0) da = (int16)da + (int16)GETREG (dr);
		PUTMEM (da, dval, srcdst);
		break;
		
	case 3:
		
		if (traceit)
			lp += sprintf (lp, " *R%d+", dr);
		da = GETREG (dr);
		PUTREG (dr, da + 2);
		PUTMEM (da, dval, srcdst);
		break;
	}
}

/***********************************************************************
* chkaddword - Check addition for carry and overflow
***********************************************************************/

static void chkaddword (int32 acc, int16 sv1, int16 sv2)
{
	CLR_CARRY;
	CLR_OVER;
	
	if ((acc & 0xFFFF) < (sv1 & 0xFFFF)) SET_CARRY;
	
	if (((sv1 & 0x8000) == (sv2 & 0x8000)) &&
		((sv2 & 0x8000) != (acc & 0x8000))) SET_OVER;
}

/***********************************************************************
* chkaddbyte - Check addition for carry and overflow
***********************************************************************/

static void chkaddbyte (int32 acc, int8 bsv1, int8 bsv2)
{
	CLR_CARRY;
	CLR_OVER;
	
	if ((acc & 0xFF) < (bsv1 & 0xFF)) SET_CARRY;
	
	if (((bsv1 & 0x80) == (bsv2 & 0x80)) &&
		((bsv2 & 0x80) != (acc & 0x80))) SET_OVER;
}

/***********************************************************************
* chksubword - Check subtraction for carry and overflow
***********************************************************************/

static void chksubword (int32 acc, int16 sv1, int16 sv2)
{
	CLR_CARRY;
	CLR_OVER;
	
	if ((uint16)sv2 >= (uint16)sv1) SET_CARRY;
	
	if (((sv1 & 0x8000) != (sv2 & 0x8000)) &&
		((sv2 & 0x8000) != (acc & 0x8000))) SET_OVER;
}

/***********************************************************************
* chksubbyte - Check subtraction for carry and overflow
***********************************************************************/

static void chksubbyte (int32 acc, int8 bsv1, int8 bsv2)
{
	CLR_CARRY;
	CLR_OVER;
	
	if ((uint8)bsv2 >= (uint8)bsv1) SET_CARRY;
	
	if (((bsv1 & 0x80) != (bsv2 & 0x80)) &&
		((bsv2 & 0x80) != (acc & 0x80))) SET_OVER;
}

/***********************************************************************
* chkparity - Check parity of a byte.
***********************************************************************/

static void chkparity (uint8 bval)
{
	int c;
	int i;
	
	for (c = 0, i = 0; i < 8; i++)
	{
		c = c + (bval & 0x01);
		bval = bval >> 1;
	}
	if (c & 0x01) SET_ODDP;
	else CLR_ODDP;
}

/***********************************************************************
* cmpbytezero - Compare byte value to zero.
***********************************************************************/

void cmpbytezero (uint8 bval)
{
	if (bval > 0) SET_LGT;
	else CLR_LGT;
	if ((int8)bval > 0) SET_AGT;
	else CLR_AGT;
	if (bval == 0) SET_EQ;
	else CLR_EQ;
}

/***********************************************************************
* cmpwordzero - Compare value to zero.
***********************************************************************/

void cmpwordzero (uint16 val)
{
	if (val > 0) SET_LGT;
	else CLR_LGT;
	if ((int16)val > 0) SET_AGT;
	else CLR_AGT;
	if (val == 0) SET_EQ;
	else CLR_EQ;
}

/***********************************************************************
* proc1op - Process type 1 operations.
***********************************************************************/

uint8 cfregs[8];
extern int cffile, cf_dsk0, cf_dsk1, cf_dsk2, mdexmode;

#include <unistd.h>
#include <fcntl.h>

static int proc1op (uint16 inst)
{
	int32 acc;
	uint16 st;
	uint16 sr;
	uint16 val;
	uint16 cval;
	uint16 dt;
	uint16 dr;
	uint16 op;
	int16 sv1, sv2;
	uint16 uv1;
	uint8 bval;
	uint8 bcval;
	int8 bsv1, bsv2;
	uint8 buv1;
	uint16 sa;

#ifdef MDEX
	uint32 dsk0, dsk1;

	if (cffile) {
		/* Fetch base sector number of FD0.DSK and FD1.DSK */
		dsk0 = __builtin_bswap32(*((uint32*)(memory+0xff00)));
		dsk1 = __builtin_bswap32(*((uint32*)(memory+0xff04)));
	}
#endif

	sr =  inst & 0x000F;
	st = (inst & 0x0030) >> 4;
	dr = (inst & 0x03C0) >> 6;
	dt = (inst & 0x0C00) >> 10;
	op = (inst & 0xF000) >> 12;
	
	switch ((inst & 0xF000) >> 12)
	{
	case 4: /* SZC */
		
		if (traceit) lp += sprintf (lp, "SZC");
		cval = getword (sr, st, TRUE, SRC);
		uv1 = getword (dr, dt, FALSE, DST);
		val = uv1 & (~cval);
		putword (dr, dt, val, DST);
		cmpwordzero (val);
		if (traceit)
			lp += sprintf (lp, " >%04X & >%04X = >%04X", cval, uv1, val);
		break;
		
	case 5: /* SZCB */
		
		if (traceit) lp += sprintf (lp, "SZCB");
		bcval = getbyte (sr, st, TRUE, SRC);
		buv1 = getbyte (dr, dt, FALSE, DST);
		bval = buv1 & (~bcval);
		putbyte (dr, dt, bval, DST);
		cmpbytezero (bval);
		chkparity (bval);
		if (traceit)
			lp += sprintf (lp, " >%02X & >%02X = >%02X", bcval, buv1, bval);
		break;
		
	case 6: /* S */
		
		if (traceit) lp += sprintf (lp, "S");
		sv1 = (int16)getword (sr, st, TRUE, SRC);
		acc = sv1;
		sv2 = (int16)getword (dr, dt, FALSE, DST);
		acc = sv2 - acc;
		uv1 = acc & 0xFFFF;
		acc = (int16)uv1;
		chksubword (acc, sv1, sv2);
		val = acc & 0xFFFF;
		putword (dr, dt, val, DST);
		cmpwordzero (val);
		if (traceit)
			lp += sprintf (lp, " >%04X - >%04X -> >%04X", sv2, sv1, acc);
		break;
		
	case 7: /* SB */
		
		if (traceit) lp += sprintf (lp, "SB");
		bsv1 = (int8)getbyte (sr, st, TRUE, SRC);
		acc = bsv1;
		bsv2 = (int8)getbyte (dr, dt, FALSE, DST);
		acc = bsv2 - acc;
		buv1 = acc & 0xFF;
		acc = (int8)buv1;
		chksubbyte (acc, bsv1, bsv2);
		bval = acc &0xFF;
		putbyte (dr, dt, bval, DST);
		cmpbytezero (bval);
		chkparity (bval);
		if (traceit)
			lp += sprintf (lp, " >%02X - >%02X -> >%02X", bsv2, bsv1, bval);
		break;
		
	case 8: /* C */
		
		if (traceit) lp += sprintf (lp, "C");
		val = getword (sr, st, TRUE, SRC);
		cval = getword (dr, dt, TRUE, DST);
		if (traceit) lp += sprintf (lp, " >%04X == >%04X", val, cval);
		if (val > cval) SET_LGT;
		else CLR_LGT;
		if ((int16)val > (int16)cval) SET_AGT;
		else CLR_AGT;
		if (val == cval) SET_EQ;
		else CLR_EQ;
		break;
		
	case 9: /* CB */
		
		if (traceit) lp += sprintf (lp, "CB");
		bval = getbyte (sr, st, TRUE, SRC);
		bcval = getbyte (dr, dt, TRUE, DST);
		if (traceit) lp += sprintf (lp, " >%02X == >%02X", bval, bcval);
		if (bval > bcval) SET_LGT;
		else CLR_LGT;
		if ((int8)bval > (int8)bcval) SET_AGT;
		else CLR_AGT;
		if (bval == bcval) SET_EQ;
		else CLR_EQ;
		chkparity (bval);
		break;
		
	case 10: /* A */
		
		if (traceit) lp += sprintf (lp, "A");
		sv1 = (int16)getword (sr, st, TRUE, SRC);
		acc = sv1;
		sv2 = (int16)getword (dr, dt, FALSE, DST);
		acc = acc + sv2;
		uv1 = acc & 0xFFFF;
		acc = (int16)uv1;
		chkaddword (acc, sv1, sv2);
		val = acc & 0xFFFF;
		putword (dr, dt, val, DST);
		cmpwordzero (val);
		if (traceit)
			lp += sprintf (lp, " >%04X + >%04X -> >%04X", sv1, sv2, acc);
		break;
		
	case 11: /* AB */
		
		if (traceit) lp += sprintf (lp, "AB");
		bsv1 = (int8)getbyte (sr, st, TRUE, SRC);
		acc = bsv1;
		bsv2 = (int8)getbyte (dr, dt, FALSE, DST);
		acc = acc + bsv2;
		buv1 = acc & 0xFF;
		acc = (int8)buv1;
		chkaddbyte (acc, bsv1, bsv2);
		bval = acc & 0xFF;
		putbyte (dr, dt, bval, DST);
		cmpbytezero (bval);
		chkparity (bval);
		if (traceit)
			lp += sprintf (lp, " >%04X + >%04X -> >%04X", bsv1, bsv2, bval);
		break;
		
	case 12: /* MOV */
		
		if (traceit) lp += sprintf (lp, "MOV");
		sa = getaddr(sr, st, 0);
		if (sa>=0xfe40 && sa<=0xfe7f) {
			int t = sa & 0x000f;
			val = (mapper[t] >> 4) | (mapper[t+1] >> 12);
			sa = getaddr(sr, st, 1);
		} else
			val = getword (sr, st, TRUE, SRC);
		sa = getaddr(dr, dt, 0);
		if (sa>=0xfe40 && sa<=0xfe7f) {
			int t = sa & 0x000f;
			mapper[t] = (val & 0xff00) << 4;
			mapper[t+1] = (val & 0xff) << 12;
			sa = getaddr(dr, dt, 1);
//			printf("map[%x] = %x\nmap[%x] = %x (val=%x)\n", t, mapper[t], t+1, mapper[t+1], val );
		} else
			putword (dr, dt, val, DST);
		cmpwordzero (val);
		if (traceit)
			lp += sprintf (lp, " val=>%04X", val);
		break;
		
	case 13: /* MOVB */
		
		if (traceit) lp += sprintf (lp, "MOVB");
		sa = getaddr(sr, st, 0);

		/* emulate CF Card read registers at location 0xfe00 */
		if (sa>=0xfe00 && sa<=0xfe07) {
			int  t = sa - 0xfe00;
			uint8 c;
			uint32 offs;
			int dskfd = (cfregs[6] & 8) ? cf_dsk1 : cf_dsk0;
			dskfd = (cfregs[6] & 16) ? cf_dsk2 : dskfd;
			switch( t ) {
			case 0:
				if (dskfd) {
					offs = read(dskfd, &c, 1);
					if (offs!=1) perror(NULL);
					bval = c;
				}
				else
					bval = 0;
				break;
			case 1:
				bval = 0x00;
				break;
			case 7:
				bval = 0x58;
				break;
			default:
				bval = cfregs[t];
			}
			getbyte (sr, st, TRUE, SRC);
		}

		/* handle byte reads to the mapper */
		else if (sa>=0xfe40 && sa<=0xfe7f) {
			int t = sa & 0x000f;
			bval = (mapper[t] >> 12) & 0xff;
			getbyte (sr, st, TRUE, SRC);
		} else
			bval = getbyte (sr, st, TRUE, SRC);

		sa = getaddr(dr, dt, 0);
		
		/* emulate CF Card write registers at location 0xfe00 */
		if (sa>=0xfe00 && sa<=0xfe07) {
			int  t = sa - 0xfe00;
			uint8 c = bval;
			int dskfd = (cfregs[6] & 8) ? cf_dsk1 : cf_dsk0;
			dskfd = (cfregs[6] & 16) ? cf_dsk2 : dskfd;
//printf("reg[%4x]<-%2x\n", sa, bval);
			switch( t ) {
			case 0:
// if (flag) printf("%d,", c);
				if (dskfd) write(dskfd, &c, 1);
				break;
			case 7: {
				uint32 offs = cfregs[3]-1 + (cfregs[6]&0xf)*32 + ((cfregs[5]<<8) + cfregs[4])*128;

				if (dskfd && (bval==0x20 || bval==0x30)) {
					//printf("h:%d,s:%d,c:%d -> lba %d\n", cfregs[6]&0x7, cfregs[3], (cfregs[5]<<8) + cfregs[4], offs);
					if (cfregs[6] & 8) {
						/* floppy has 2 heads, 16 sectors per track */
						offs = cfregs[3]-1 + (cfregs[6]&0x7)*16 + ((cfregs[5]<<8) + cfregs[4])*32;
						lseek(dskfd, offs*256, SEEK_SET);
					}
					else if (cfregs[6] & 16) {
						/* disk no. 3 */
						offs = (cfregs[3]-1 + (cfregs[6]&0x7)*32 + ((cfregs[5]<<8) + cfregs[4])*128);					
						lseek(dskfd, offs*256, SEEK_SET);
					}
					else {
						/* harddisk has 4 heads, 32 sectors per track */
						offs = mdexmode ? (cfregs[3]-1 + (cfregs[6]&0x7)*16 + ((cfregs[5]<<8) + cfregs[4])*32) :
						                  (cfregs[3]-1 + (cfregs[6]&0x7)*32 + ((cfregs[5]<<8) + cfregs[4])*128);					
						lseek(dskfd, offs*256, SEEK_SET);
					}
					break;
				}
			}
			default:
				cfregs[t] = bval;
			}
			putbyte (dr, dt, bval, DST);
		}

		/* handle byte writes to the mapper */
		else if (sa>=0xfe40 && sa<=0xfe7f) {
			int t = sa & 0x000f;
			mapper[t] = bval << 12;
			putbyte (dr, dt, bval, DST);
			//printf("map[%x] = %x (%d)\n", t, mapper[t], traceenable);
		} else
			putbyte (dr, dt, bval, DST);

		cmpbytezero (bval);
		chkparity (bval);
		if (traceit)
			lp += sprintf (lp, " bval=>%02X", bval);
		break;
		
	case 14: /* SOC */
		
		if (traceit) lp += sprintf (lp, "SOC");
		cval = getword (sr, st, TRUE, SRC);
		uv1 = getword (dr, dt, FALSE, DST);
		val = cval | uv1;
		putword (dr, dt, val, DST);
		cmpwordzero (val);
		if (traceit)
			lp += sprintf (lp, " >%04X | >%04X = >%04X", cval, uv1, val);
		break;
		
	case 15: /* SOCB */
		
		if (traceit) lp += sprintf (lp, "SOCB");
		bcval = getbyte (sr, st, TRUE, SRC);
		buv1 = getbyte (dr, dt, FALSE, DST);
		bval = bcval | buv1;
		putbyte (dr, dt, bval, DST);
		cmpbytezero (bval);
		chkparity (bval);
		if (traceit)
			lp += sprintf (lp, " >%02X | >%02X = >%02X", bcval, buv1, bval);
		break;
		
	default:
		return (-1);
   }
   return (0);
}

/***********************************************************************
* proc2op - Process type 2 operations.
***********************************************************************/

static int proc2op (uint16 inst)
{
	int32 mwpc;
	int16 wpc;
	int8 disp;
	
	disp = inst & 0x00FF;
	wpc = (pcreg >> 1) & 0x7FFF;
	mwpc = (mpcreg >> 1) & 0xFFFFF;
	
	switch ((inst & 0x0F00) >> 8)
	{
	case 0: /* JMP */
		
		if (traceit) lp += sprintf (lp, "JMP >%X", disp & 0xFF);
		break;
		
	case 1: /* JLT */
		
		if (traceit) lp += sprintf (lp, "JLT >%X", disp & 0xFF);
		if (!IS_AGT && !IS_EQ) break;
		return (0);
		
	case 2: /* JLE */
		
		if (traceit) lp += sprintf (lp, "JLE >%X", disp & 0xFF);
		if (!IS_LGT || IS_EQ) break;
		return (0);
		
	case 3: /* JEQ */
		
		if (traceit) lp += sprintf (lp, "JEQ >%X", disp & 0xFF);
		if (IS_EQ) break;
		return (0);
		
	case 4: /* JHE */
		
		if (traceit) lp += sprintf (lp, "JHE >%X", disp & 0xFF);
		if (IS_LGT || IS_EQ) break;
		return (0);
		
	case 5: /* JGT */
		
		if (traceit) lp += sprintf (lp, "JGT >%X", disp & 0xFF);
		if (IS_AGT) break;
		return (0);
		
	case 6: /* JNE */
		
		if (traceit) lp += sprintf (lp, "JNE >%X", disp & 0xFF);
		if (!IS_EQ) break;
		return (0);
		
	case 7: /* JNC */
		
		if (traceit) lp += sprintf (lp, "JNC >%X", disp & 0xFF);
		if (!IS_CARRY) break;
		return (0);
		
	case 8: /* JOC */
		
		if (traceit) lp += sprintf (lp, "JOC >%X", disp & 0xFF);
		if (IS_CARRY) break;
		return (0);
		
	case 9: /* JNO */
		
		if (traceit) lp += sprintf (lp, "JNO >%X", disp & 0xFF);
		if (!IS_OVER) break;
		return (0);
		
	case 10: /* JL */
		
		if (traceit) lp += sprintf (lp, "JL >%X", disp & 0xFF);
		if (!IS_LGT && !IS_EQ) break;
		return (0);
		
	case 11: /* JH */
		
		if (traceit) lp += sprintf (lp, "JH >%X", disp & 0xFF);
		if (IS_LGT && !IS_EQ) break;
		return (0);
		
	case 12: /* JOP */
		
		if (traceit) lp += sprintf (lp, "JOP >%X", disp & 0xFF);
		if (IS_ODDP) break;
		return (0);
		
	case 13: /* SBO */
		
		if (traceit) lp += sprintf (lp, "SBO >%X", disp & 0xFF);
		if (traceit) lp += sprintf (lp, " BASE >%04X", R12);
		setbitone (inst);
		return (0);
		
	case 14: /* SBZ */
		
		if (traceit) lp += sprintf (lp, "SBZ >%X", disp & 0xFF);
		if (traceit) lp += sprintf (lp, " BASE >%04X", R12);
		setbitzero (inst);
		return (0);
		
	case 15: /* TB */
		
		if (traceit) lp += sprintf (lp, "TB >%X", disp & 0xFF);
		if (traceit) lp += sprintf (lp, " BASE >%04X", R12);
		testbit (inst);
		return (0);
		
	default:
		return (-1);
   }
   
   /*
   ** Fall through, take the jump
   */
   
   if ((uint8)disp == 0xFF)
   {
	   idle = TRUE;
	   if (GET_MASK == 0) run = FALSE;
   }
   else
	   opcreg = pcreg;
   
   wpc += disp;
   mwpc += disp;
   pcreg = (wpc << 1) & 0xFFFE;
   mpcreg = (mwpc << 1) & 0x1FFFFE;
   return (0);
}

/***********************************************************************
* proc3op - Process type 3 operations.
***********************************************************************/
void mdex_call(int op, int wp, int pc, int st);

static int proc3op (uint16 inst)
{
	uint16 st;
	uint16 sr;
	uint16 reg;
	uint16 val;
	uint16 cval;
	uint16 op;
	uint8 svc;
	
	sr =  inst & 0x000F;
	st = (inst & 0x0030) >> 4;
	reg = (inst & 0x03C0) >> 6;
	op = (inst & 0x1C00) >> 10;
	
	switch (op)
	{
	case 0: /* COC */
		
		if (traceit) lp += sprintf (lp, "COC");
		val = getword (sr, st, TRUE, SRC);
		if (traceit)
			lp += sprintf (lp, " R%d", reg);
		cval = val & GETREG (reg);
		if (cval == val) SET_EQ;
		else CLR_EQ;
		if (traceit)
			lp += sprintf (lp, " val=>%04X & cval=>%04X", val, GETREG(reg));
		break;
		
	case 1: /* CZC */
		
		if (traceit) lp += sprintf (lp, "CZC");
		val = getword (sr, st, TRUE, SRC);
		if (traceit)
			lp += sprintf (lp, " R%d", reg);
		cval = val & (~GETREG (reg));
		if (cval == val) SET_EQ;
		else CLR_EQ;
		if (traceit)
			lp += sprintf (lp, " val=>%04X & cval=>%04X", val, GETREG(reg));
		break;
		
	case 2: /* XOR */
		
		if (traceit) lp += sprintf (lp, "XOR");
		val = getword (sr, st, TRUE, SRC);
		if (traceit)
			lp += sprintf (lp, " R%d", reg);
		val ^= GETREG (reg);
		PUTREG (reg, val);
		cmpwordzero (val);
		if (traceit)
			lp += sprintf (lp, " val=>%04X ^ cval=>%04X",
			val, (~GETREG(reg) & 0xFFFF));
		break;
		
	case 3: /* XOP */
		
		if (traceit) lp += sprintf (lp, "XOP");
		val = wpreg;
		cval = getaddr (sr, st, TRUE);
		if (traceit)
			lp += sprintf (lp, " %d", reg);
#ifdef HOST
		if (reg == 1) /* SYSCALL */
		{
			if (traceit)
				lp += sprintf (lp, " SYS >%02X", cval);
			mdex_call(cval, wpreg, pcreg, statreg);
			break;
		}
#endif
#ifdef UNIX
		if (reg == 1 && ptraceenable) /* SYSCALL */
		{
			lp += sprintf (lp, " SYS %02d\n", cval);
		}
#endif
		if (reg == 15) /* SVC */
		{
			svc = GETMEMB (cval, SRC);
			if (traceit)
				lp += sprintf (lp, " SVC >%02X", svc);
			if (svc == 2) 
			{
				uint16 delay = GETMEM (cval+2, SRC);
				if (traceit)
					lp += sprintf (lp, " Delay %d", delay);
				if (delay == 0) PUTMEM (cval+2, 1, SRC);
			}
		}
		op = statreg;
		SET_XOP;
		CLR_NONPRV; /* Privileged */
		CLR_MAP; /* Use Map 0 */
		CLR_OVERI;
		CLR_WCS;
		wpreg = GETMEM0 (reg * 4 + 0x0040) & 0xFFFE;
		PUTREG (11, cval);
		cval = pcreg;
		mpcreg = pcreg = GETMEM0 (reg * 4 + 0x0042) & 0xFFFE;
		PUTREG (13, val);
		PUTREG (14, cval);
		PUTREG (15, op);
		deferint = 1;
		break;
		
	default:
		return (-1);
	}
	return (0);
}

/***********************************************************************
* proc4op - Process type 4 operations.
***********************************************************************/

static int proc4op (uint16 inst)
{
	uint16 sr;
	uint16 st;
	uint16 cnt;
	uint16 dat;
	uint8 bdat;
	
	sr = inst & 0x000F;
	st = (inst & 0x0030) >> 4;
	cnt = (inst & 0x03C0) >> 6;
	
	switch ((inst & 0x0400) >> 10)
	{
	case 0: /* LDCR */
		
		if (traceit) lp += sprintf (lp, "LDCR");
		if (cnt > 1 && cnt < 9)
		{
			bdat = getbyte (sr, st, TRUE, SRC);
			if (traceit) lp += sprintf (lp, ",%d DAT >%02X", cnt, bdat);
			cmpbytezero (bdat);
			chkparity (bdat);
			dat = bdat;
		}
		else
		{
			dat = getword (sr, st, TRUE, SRC);
			if (traceit) lp += sprintf (lp, ",%d DAT >%04X", cnt, dat);
			cmpwordzero (dat);
		}
		if (traceit) lp += sprintf (lp, " BASE >%04X", R12);
		loadcru (inst, dat);
		break;
		
	case 1: /* STCR */
		
		if (traceit) lp += sprintf (lp, "STCR");
		dat = storecru (inst);
		if (cnt > 1 && cnt < 9)
		{
			bdat = (uint8)dat;
			putbyte (sr, st, bdat, SRC);
			if (traceit) lp += sprintf (lp, ",%d DAT >%02X", cnt, bdat);
			cmpbytezero (bdat);
			chkparity (bdat);
		}
		else
		{
			putword (sr, st, dat, SRC);
			if (traceit) lp += sprintf (lp, ",%d DAT >%04X", cnt, dat);
			cmpwordzero (dat);
		}
		if (traceit) lp += sprintf (lp, " BASE >%04X", R12);
		break;
		
	default:
		return (-1);
	}
	return (0);
}

/***********************************************************************
* proc5op - Process type 5 operations.
***********************************************************************/

static int proc5op (uint16 inst)
{
	uint32 acc;
	uint16 reg;
	uint16 cnt;
	uint16 val;
	uint16 sbit;
	
	reg = inst & 0x000F;
	cnt = (inst & 0x00F0) >> 4;
	if (cnt == 0)
	{
		cnt = R0 & 0x000F;
		if (cnt == 0) cnt = 16;
	}
	val = GETREG (reg);
	
	switch ((inst & 0x0300) >> 8)
	{
	case 0: /* SRA */
		
		if (traceit) lp += sprintf (lp, "SRA R%d %d", reg, cnt);
		acc = val;
		if (traceit)
			lp += sprintf (lp, " val=>%04X", val);
		acc = acc << 16;
		acc = (int32)acc >> cnt;
		if (acc & 0x00008000) SET_CARRY;
		else CLR_CARRY;
		val = (acc >> 16) & 0xFFFF;
		PUTREG (reg, val);
		cmpwordzero (val);
		if (traceit)
			lp += sprintf (lp, " val=>%04X", val);
		break;
		
	case 1: /* SRL */
		
		if (traceit) lp += sprintf (lp, "SRL R%d %d", reg, cnt);
		acc = val;
		if (traceit)
			lp += sprintf (lp, " val=>%04X", val);
		acc = acc << 16;
		acc = acc >> cnt;
		if (acc & 0x00008000) SET_CARRY;
		else CLR_CARRY;
		val = (acc >> 16) & 0xFFFF;
		PUTREG (reg, val);
		cmpwordzero (val);
		if (traceit)
			lp += sprintf (lp, " val=>%04X", val);
		break;
		
	case 2: /* SLA */
		
		if (traceit) lp += sprintf (lp, "SLA R%d %d", reg, cnt);
		CLR_OVER;
		acc = val;
		if (traceit)
			lp += sprintf (lp, " val=>%04X", val);
		sbit = acc & 0x8000;
		while (cnt)
		{
			acc = acc << 1;
			if (sbit != (acc & 0x8000)) SET_OVER;
			sbit = acc & 0x8000;
			cnt--;
		}
		if (acc & 0x10000) SET_CARRY;
		else CLR_CARRY;
		val = acc & 0xFFFF;
		PUTREG (reg, val);
		cmpwordzero (val);
		if (traceit)
			lp += sprintf (lp, " val=>%04X", val);
		break;
		
	case 3: /* SRC */
		
		if (traceit) lp += sprintf (lp, "SRC R%d %d", reg, cnt);
		if (traceit)
			lp += sprintf (lp, " val=>%04X", val);
		while (cnt)
		{
			uint16 ob;
			
			ob = val & 0x0001;
			val = val >> 1;
			if (ob)
			{
				val = val | 0x8000;
				SET_CARRY;
			}
			else CLR_CARRY;
			cnt--;
		}
		PUTREG (reg, val);
		cmpwordzero (val);
		if (traceit)
			lp += sprintf (lp, " val=>%04X", val);
		break;
		
	default:
		return (-1);
	}
	return (0);
}

/***********************************************************************
* proc6op - Process type 6 operations.
***********************************************************************/

static int proc6op (uint16 inst)
{
	int32 acc;
	uint16 sr;
	uint16 st;
	uint16 val;
	int16 ival;
	uint16 sval;
	uint16 xinst;
	
	sr = inst & 0x000F;
	st = (inst & 0x0030) >> 4;
	
	if ((inst & 0x0F00) >= 0x0C00) /* Floating point instructions */
	{
		return (-1);
	}
	else if ((inst & 0x0F00) == 0x0100) 
	{
		switch ((inst & 0x00C0) >> 6)
		{
		case 1: /* BIND */
		case 2: /* DIVS */
			if (traceit) lp += sprintf (lp, "DIVS");
			ival = getword (sr, st, TRUE, SRC);
			acc = GETREG (0);
			acc = (acc << 16) | GETREG (1);
			PUTREG (0, acc / ival);
			PUTREG (1, acc % ival);
			break;
		case 3: /* MPYS */
		default:
			return (-1);
		}
	}
	else switch ((inst & 0x03C0) >> 6)
	{
   case 0: /* BLWP */
	   
	   if (traceit) lp += sprintf (lp, "BLWP");
	   val = wpreg;
	   if (st == 0)
	   {
		   if (traceit)
			   lp += sprintf (lp, " R%d", sr);
		   sval = GETREG (sr+1);
		   wpreg = GETREG (sr) & 0xFFFE;
	   }
	   else
	   {
		   sval = getaddr (sr, st, TRUE);
		   wpreg = GETMEM (sval, SRC) & 0xFFFE;
		   sval = GETMEM (sval+2, SRC);
	   }
	   PUTREG (13, val);
	   PUTREG (14, pcreg);
	   PUTREG (15, statreg);
	   opcreg = pcreg;
	   mpcreg = pcreg = sval & 0xFFFE;
	   deferint = 1;
	   break;
	   
   case 1: /* B */
	   
	   if (traceit) lp += sprintf (lp, "B");
	   val = getaddr (sr, st, TRUE);
	   opcreg = pcreg;
	   mpcreg = pcreg = val & 0xFFFE;
	   break;
	   
   case 2: /* X */
	   
	   if (traceit) lp += sprintf (lp, "X");
	   val = getaddr (sr, st, TRUE);
	   xinst = GETMEM (val, SRC);
	   if (traceit) lp += sprintf (lp, "\n ");
	   runinst (xinst);
	   break;
	   
   case 3: /* CLR */
	   
	   if (traceit) lp += sprintf (lp, "CLR");
	   putword (sr, st, 0, SRC);
	   break;
	   
   case 4: /* NEG */
	   
	   if (traceit) lp += sprintf (lp, "NEG");
	   sval = val = getword (sr, st, FALSE, SRC);
	   val = -(int16)val;
	   if (val == 0) SET_CARRY;
	   else CLR_CARRY;
	   if (sval == 0x8000) SET_OVER;
	   else CLR_OVER;
	   if (traceit)
		   lp += sprintf (lp, " ~>%04X + 1 -> >%04X", sval, val);
	   goto SAVE_CHECK_ZERO;
	   
   case 5: /* INV */
	   
	   if (traceit) lp += sprintf (lp, "INV");
	   val = ~getword (sr, st, FALSE, SRC);
	   goto SAVE_CHECK_ZERO;
	   
   case 6: /* INC */
	   
	   if (traceit) lp += sprintf (lp, "INC");
	   val = getword (sr, st, FALSE, SRC);
	   acc = (int16)val;
	   sval = 1;
	   goto CHECK_INC_STATUS;
	   
   case 7: /* INCT */
	   
	   if (traceit) lp += sprintf (lp, "INCT");
	   val = getword (sr, st, FALSE, SRC);
	   acc = (int16)val;
	   sval = 2;
	   
CHECK_INC_STATUS:
	   acc += sval;
	   if (traceit)
		   lp += sprintf (lp, "  >%04X + %d -> >%04X", val, sval, acc);
	   chkaddword (acc, (int16)val, (int16)sval);
	   val = acc & 0xFFFF;
	   goto SAVE_CHECK_ZERO;
	   
	   
   case 8: /* DEC */
	   
	   if (traceit) lp += sprintf (lp, "DEC");
	   val = getword (sr, st, FALSE, SRC);
	   acc = (int16)val;
	   sval = 1;
	   goto CHECK_DEC_STATUS;
	   
   case 9: /* DECT */
	   
	   if (traceit) lp += sprintf (lp, "DECT");
	   val = getword (sr, st, FALSE, SRC);
	   acc = (int16)val;
	   sval = 2;
	   
CHECK_DEC_STATUS:
	   acc -= sval;
	   if (traceit)
		   lp += sprintf (lp, "  >%04X - %d -> >%04X", val, sval, acc);
	   chksubword (acc, (int16)sval, (int16)val);
	   val = acc & 0xFFFF;
	   
SAVE_CHECK_ZERO:
	   putword (sr, st, val, SRC);
	   cmpwordzero (val);
	   break;
	   
   case 10: /* BL */
	   
	   if (traceit) lp += sprintf (lp, "BL");
	   val = getaddr (sr, st, TRUE);
	   PUTREG (11, pcreg);
	   opcreg = pcreg;
	   mpcreg = pcreg = val & 0xFFFE;
	   break;
	   
   case 11: /* SWPB */
	   
	   if (traceit) lp += sprintf (lp, "SWPB");
	   val = getword (sr, st, FALSE, SRC);
	   sval = val >> 8;
	   sval |= (val & 0xFF) << 8;
	   putword (sr, st, sval, SRC);
	   break;
	   
   case 12: /* SETO */
	   
	   if (traceit) lp += sprintf (lp, "SETO");
	   putword (sr, st, 0xFFFF, SRC);
	   break;
	   
   case 13: /* ABS */
	   
	   if (traceit) lp += sprintf (lp, "ABS");
	   CLR_OVER;
	   CLR_CARRY;
	   sval = val = getword (sr, st, FALSE, SRC);
	   cmpwordzero (val);
	   if ((int16)val < 0)
	   {
		   if (val == 0x8000) SET_OVER;
		   val = -(int16)val;
	   }
	   if (traceit)
		   lp += sprintf (lp, " abs(>%04X) -> >%04X", sval, val);
	   putword (sr, st, val, SRC);
	   deferint = 1;
	   break;
	   
   case 14: /* LDS */
   case 15: /* LDD */
   default:
	   return (-1);
   }
   return (0);
}

/***********************************************************************
* proc7op - Process type 7 operations.
***********************************************************************/
int lrexflag;

static int proc7op (uint16 inst)
{
	uint16 val;
	
	switch ((inst & 0x00F0) >> 4)
	{
		case 0: /* Floating point instructions */
		case 2:
		case 4: /* IDLE */
			
			if (traceit) lp += sprintf (lp, "IDLE");
			idle = TRUE;
			idleled = TRUE;
			if (GET_MASK == 0) run = FALSE;
			instcount--;
			pcreg -= 2;
			mpcreg -= 2;
			break;
			
		case 6: /* RSET */
			
			if (traceit) lp += sprintf (lp, "RSET");
			SET_MASK (0);
			errcrudata = 0;
			mapcrudata = 0;
			maplatch = 0;
			mapenabled = FALSE;
			intsenabled = TRUE;
			break;
			
		case 8: /* RTWP */
			
			if (traceit) lp += sprintf (lp, "RTWP");
			val = R13 & 0xFFFE;
			mpcreg = pcreg = R14 & 0xFFFE;
			statreg = R15;
			wpreg = val;
			if (traceit)
				lp += sprintf (lp, " wp=>%04X pc=>%04X st=>%04X",
				wpreg, pcreg, statreg);
			break;
			
		case 10: /* CKON */
			
			if (traceit) lp += sprintf (lp, "CKON");
//traceenable = 1;
			break;
			
		case 12: /* CKOF */
			
			if (traceit) lp += sprintf (lp, "CKOF");
//traceenable = 0;
//printf("*\n");
			flag = 0;
			break;
			
		case 14: /* LREX */
			if (traceit) lp += sprintf (lp, "LREX");
			lrexflag = 3;
			break;
			
		default:
			return (-1);
   }
   return (0);
}

/***********************************************************************
* proc8op - Process type 8 operations.
***********************************************************************/

static int proc8op (uint16 inst)
{
	int32 acc;
	uint16 r;
	uint16 val;
	uint16 cval;
	uint16 tval;
	int32 sv1, sv2;
	
	r = inst & 0x000F;
	
	if ((inst & 0xFFF0) == 0x00B0) /* BLSK */
	{
		return (-1);
	}
	else if ((inst & 0xFFF0) == 0x0080) /* LST */
	{
	   if (traceit) lp += sprintf (lp, "LST R%d", r);
	   statreg = GETREG (r);
	}
	else if ((inst & 0xFFF0) == 0x0090) /* LWP */
	{
	   if (traceit) lp += sprintf (lp, "LWP R%d", r);
	   wpreg = GETREG (r);
	}
	else if (inst == 0x0300) /* LIMI */
	{
		SET_MASK (GETINST);
		if (traceit) lp += sprintf (lp, "LIMI %d", GET_MASK);
		pcreg += 2;
		mpcreg += 2;
	}
	
	else switch ((inst & 0x00F0) >> 4)
	{
    case 0: /* LI */
	   
	   val = GETINST;
	   pcreg += 2;
	   mpcreg += 2;
	   if (traceit) lp += sprintf (lp, "LI R%d >%04X", r, val);
	   PUTREG (r, val);
	   cmpwordzero (val);
	   break;
	   
    case 2: /* AI */
	   
	   sv1 = GETINST;
	   pcreg += 2;
	   mpcreg += 2;
	   cval = sv1;
	   if (traceit) lp += sprintf (lp, "AI R%d >%04X", r, cval);
	   acc = sv1;
	   tval = GETREG (r);
	   sv2 = tval;
	   acc = acc + sv2;
	   val = acc & 0xFFFF;
	   acc = (int16)val;
	   chkaddword (acc, sv1, sv2);
	   val = acc & 0xFFFF;
	   PUTREG (r, val);
	   cmpwordzero (val);
	   if (traceit)
		   lp += sprintf (lp, " >%04X + >%04X = >%04X", cval, tval, val);
	   break;
	   
    case 4: /* ANDI */
	   
	   cval = GETINST;
	   pcreg += 2;
	   mpcreg += 2;
	   if (traceit) lp += sprintf (lp, "ANDI R%d >%04X", r, cval);
	   tval = GETREG (r);
	   val = tval & cval;
	   PUTREG (r, val);
	   cmpwordzero (val);
	   if (traceit)
		   lp += sprintf (lp, " >%04X & >%04X = >%04X", cval, tval, val);
	   break;
	   
    case 6: /* ORI */
	   
	   cval = GETINST;
	   pcreg += 2;
	   mpcreg += 2;
	   if (traceit) lp += sprintf (lp, "ORI R%d >%04X", r, cval);
	   tval = GETREG (r);
	   val = tval | cval;
	   PUTREG (r, val);
	   cmpwordzero (val);
	   if (traceit)
		   lp += sprintf (lp, " >%04X | >%04X = >%04X", cval, tval, val);
	   break;
	   
    case 8: /* CI */
	   
	   cval = GETINST;
	   pcreg += 2;
	   mpcreg += 2;
	   if (traceit) lp += sprintf (lp, "CI R%d >%04X", r, cval);
	   val = GETREG (r);
	   if (val > cval) SET_LGT;
	   else CLR_LGT;
	   if ((int16)val > (int16)cval) SET_AGT;
	   else CLR_AGT;
	   if (val == cval) SET_EQ;
	   else CLR_EQ;
	   if (traceit)
		   lp += sprintf (lp, " >%04X == >%04X", cval, val);
	   break;
	   
    case 10: /* STWP */
	   
	   if (traceit) lp += sprintf (lp, "STWP R%d", r);
	   PUTREG (r, wpreg);
	   break;
	   
    case 12: /* STST */
	   
	   if (traceit) lp += sprintf (lp, "STST R%d", r);
	   PUTREG (r, statreg);
	   break;
	   
    case 14: /* LWPI */
	   
	   val = wpreg;
	   wpreg = GETINST & 0xFFFE;
	   if (traceit) lp += sprintf (lp, "LWPI >%04X", wpreg);
	   pcreg += 2;
	   mpcreg += 2;
	   break;
	   
    default:
	   return (-1);
	}
	return (0);
}

/***********************************************************************
* proc9op - Process type 9 operations.
***********************************************************************/

static int proc9op (uint16 inst)
{
	uint32 acc;
	uint16 val;
	uint16 st;
	uint16 sr;
	uint16 r;
	uint16 uval;
	uint16 op;
	
	sr =  inst & 0x000F;
	st = (inst & 0x0030) >> 4;
	r = (inst & 0x03C0) >> 6;
	op = (inst & 0x0400) >> 10;
	
	switch ((inst & 0x0400) >> 10)
	{
	case 0: /* MPY */
		
		if (traceit) lp += sprintf (lp, "MPY");
		val = getword (sr, st, TRUE, SRC);
		if (traceit)
			lp += sprintf (lp, " R%d ", r);
		acc = GETREG (r);
		if (traceit)
			lp += sprintf (lp, "%04x x %04x ", acc, val);
		acc = acc * (uint32)val;
		uval = (acc >> 16) & 0xFFFF;
		if (traceit)
			lp += sprintf (lp, "=> %8x ", acc);
		PUTREG (r, uval);
		uval = acc & 0xFFFF;
		PUTREG (r+1, uval);
		break;
		
	case 1: /* DIV */
		
		if (traceit) lp += sprintf (lp, "DIV");
		val = getword (sr, st, TRUE, SRC);
		if (traceit)
			lp += sprintf (lp, " R%d", r);
		if (val <= GETREG (r))
		{
			SET_OVER;
		}
		else
		{
			CLR_OVER;
			acc = GETREG (r);
			acc = (acc << 16) | GETREG (r+1);
			PUTREG (r, acc / val);
			PUTREG (r+1, acc % val);
		}
		break;
		
	default:
		return (-1);
	}
	return (0);
}

/***********************************************************************
* proc10op - Process type 10 operations.
***********************************************************************/

static int proc10op (uint16 inst)
{
	uint16 r;
	uint16 mf;
	
	r =  inst & 0x000F;
	mf = (inst & 0x0010) >> 4;
	if (traceit) lp += sprintf (lp, "LMF R%d,%d", r, mf);
	return (-1);
}

/***********************************************************************
* runinst - Run an instruction.
***********************************************************************/

static void runinst (uint16 inst)
{
	int error;
	int type, i;
	
/*	if (traceenable && pcreg >= tracelim[0] && pcreg <= tracelim[1] &&
		instcount >= tracestart && (GET_MASK == 0 || GET_MASK == 15))
		traceit = TRUE;
	else
		traceit = FALSE;
*/	
		traceit = traceenable;
/*
        if (mapaddr(pcreg-2) >= 0x21000 && mapaddr(pcreg-2) <= 0x2ffff)
		traceit = TRUE;
	else
		traceit = FALSE;
*/
	if (traceit) {
                char *op;
		op = lp = &lines[ii++][0];
                *lp = 0;
                if( ii>=TRACSIZ ) ii = 0;
		lp += sprintf (lp, "%ld: PC %04X(%06X) WP %04X ST %04X INST %04X ",
		instcount, pcreg-2, mapaddr(pcreg-2), wpreg, statreg, inst);
        }
	
	if (inst != IDLEINST) idleled = FALSE;
	curinst = inst;
	type = decodeinst (inst);
	if (type>9) {
		printf("Error inst type > 9\n");
                for(i=ii; i<TRACSIZ; i++)
                        if( lines[i][0] ) printf("%s\n", lines[i]);
                for(i=0; i<ii; i++)
                        printf("%s\n", lines[i]);                        
		for(i=0; i<16; i++) {
			printf("map[%x] = %02x\n", i, mapper[i]>>12);
		}
		exit(1);
	}
	
	deferint = 0;
	error = -1;
	
	if (type < MAX_INST_TYPES)
		error = inst_proc[type].proc (inst);
	
	if (error < 0)
	{
		sprintf (view[0], "Illegal instruction: inst = >%04X, pc = >%04X",
		inst, pcreg-2);
	}
	
	instcount++;
	if (traceit) {
		fprintf (stdout, "%s\n", lines[ii-1]);
	}
	
	inhibitwrite = FALSE;
	
}

/***********************************************************************
* stepprog - Step through a program.
***********************************************************************/

void stepprog (void)
{
	uint16 inst;
	int i;

	/* perform delayed nmi on lrex */
	switch(lrexflag) {
	case 2: case 3:
		lrexflag--;
		break;
	case 1:
		lrexflag = 0;
		CLR_MAP;
		i = wpreg;
		wpreg = GETMEM0 (0xFFFC) & 0xFFFE;
		PUTREG (13, i);
		PUTREG (14, pcreg);
		PUTREG (15, statreg);
		mpcreg = pcreg = GETMEM0 (0xFFFE) & 0xFFFE;
	}

	inst   = GETINST;
	pcreg  = (pcreg + 2) & 0xFFFE;
	mpcreg = (mpcreg + 2) & 0x1FFFFE;
	runinst (inst);
// for(i=0; i<800; i++) ;
//	usleep(1);
}

/***********************************************************************
* runprog - Run a program.
***********************************************************************/

void mdex_init();
void init_acas();
void  checkinterrupts (void);

void runprog (void)
{
	int i;

	init_acas();

#ifdef HOST
	/* Init MDEX system call emulation */
	mdex_init();
#endif
	
	while (run)
	{
		idle = FALSE;
		runled = TRUE;
		stepprog ();
		if (breakflag && mpcreg == breakaddr)
		{
			run = FALSE;
			runled = FALSE;
		}
		else
		{
			if (!deferint)
				checkinterrupts ();
		}
	}
}


