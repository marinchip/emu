/***********************************************************************
*
* sim990.c - Simulator for the TI 990 computer.
*
* Changes:
*   05/29/03   DGP   Original.
*   06/20/03   DGP   Added interrupt support.
*   12/09/03   DGP   Changed ROM load to >FC00.
*   12/10/03   DGP   Added 20 bit addresses for ROM and TILINE.
*   01/26/04   DGP   Added memory size option.
*   05/10/04   DGP   Added DEV_NULL support.
*   07/01/04   DGP   Added /10A cpu, model = 11.
*   04/01/05   DGP   Changed signal handling.
*   04/12/05   DGP   Changed CONOUT to use prtview in panel mode and
*                    view mode to support multi screens.
*   04/20/05   DGP   Ensure args don't exceed argc.
*   12/04/05   DGP   Added upcase switch on CONIN.
*   10/01/07   DGP   Added realtime clock arg.
*
***********************************************************************/

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <memory.h>
#include <ctype.h>
#include <signal.h>
#include <errno.h>
#if defined(UNIX) || defined(MACH)
#include <unistd.h>
#endif
#include <fcntl.h>

#include "simdef.h"

uint16 pcreg;			/* The program PC */
uint16 opcreg;			/* The old program PC */
uint16 statreg;			/* The program status register */
uint16 wpreg;			/* The program Workspace Pointer */
uint16 lights;			/* The panel lights */
uint32 mpcreg;			/* Mapped PC */
uint32 breakaddr;		/* Break point address */
long pancount;			/* Instructions executed for panel */
unsigned long instcount;	/* Instructions executed */
unsigned long mipcount;		/* Instructions executed for MIP calc */

int run = FALSE;		/* CPU running */
int idle = FALSE;		/* CPU running IDLE or JXX $ */
int runled = FALSE;		/* CPU running LED */
int idleled = FALSE;		/* CPU idle LED */
int faultled = FALSE;		/* CPU fault LED */
int enablepanel = FALSE;	/* Enable panel */
int breakflag = FALSE;		/* No breakpoint enabled */
int traceenable = FALSE;	/* No trace enabled */
int ptraceenable = FALSE;	/* No ptrace enabled */
int traceit = FALSE;		/* No trace enabled */
int mapenabled = FALSE;		/* Memory mapping enabled */
int intsenabled = TRUE;		/* Interrupts enabled */
int trclatch = FALSE;		/* Error Trace latch */
int errindex = 0;		/* Error Trace Memory index */
int trcindex = 0;		/* Error Trace Memory locked index */
int devcnt = 0;			/* Attached device count */
int model = 4;			/* Which model */
int deferint = 0;		/* Defer interrupts inst count */
int tracelim[2];		/* Trace memory limits */
int tracestart = 0;		/* Trace instruction count start */
int traceend = 0;		/* Trace instruction count end */
int windowlen = 25;		/* Window length */
int viewlen;			/* View length */
int prtviewlen;			/* prtview length */
int prtviewcol;			/* prtview column */
int usertclock = FALSE;		/* Use Real Time clock */

uint16 mapcrudata = 0;		/* Memory map file CRU >1FA0 */
uint16 curmapindex = 0;		/* Current map index */
uint16 errcrudata = 0;		/* ERR CRU >1FC0 data */
uint16 curinst = 0;		/* Current instruction */

uint32 maplatch = 0;		/* Memory map latch */
uint32 memlen = MEMSIZE;	/* Length of system memory */
long delayclock = 0;		/* Delay clock interval */

char romfile[256];		/* ROM file name */
char view[MAXVIEW][81];		/* Messages panel */
char prtview[MAXVIEW][81];	/* Print messages panel */
uint8 memory[MEMSIZE];		/* The CPU memory */
MapFile mapfile[MAPSIZE];	/* Memory Mapping Files */
FILE *tracefd = NULL;		/* Trace file fd */
uint32 tracemem[16];		/* Error Trace Memory locked */
uint32 errtrace[16];		/* Error Trace Memory /12 only */
extern uint16 intrequest;

/***********************************************************************
* clearall - Clear out system.
***********************************************************************/

int clearall ()
{
	/*
	** Clear registers
	*/
	
	mpcreg = pcreg = 0;
	statreg = 0;
	wpreg = 0;
	lights = 0;
	errcrudata = 0;
	mapcrudata = 0;
	mapenabled = FALSE;
	
	/*
	** Clear memory
	*/
	
	memset (&memory, 0x55, sizeof (memory));
	memset (&(memory[0xf000]), 0x00, 0x1000);
	
	instcount = 0;
		
	return (0);
}

/***********************************************************************
* sigintr - Process interrupt signal.
***********************************************************************/

void
sigintr (int sig)
{
   run = FALSE;
   signal (SIGINT, sigintr);
}

int cffile, cf_dsk0, cf_dsk1, cf_dsk2;

#include <termios.h>
#include <unistd.h>
#include <fcntl.h>

struct termios oldt, newt, siot;
int oldf;

int usbsiofd;
int mdexmode;
char *path;

/* enter "semi-cooked" mode */
void mkrare()
{
	newt = oldt;
	newt.c_lflag &= ~(ICANON | ECHO | ISIG);
	newt.c_iflag &= ~(IXON);
	tcsetattr(STDIN_FILENO, TCSANOW, &newt);
	oldf = fcntl(STDIN_FILENO, F_GETFL, 0);
	fcntl(STDIN_FILENO, F_SETFL, oldf | O_NONBLOCK);
}

void treset()
{
	tcsetattr(STDIN_FILENO, TCSANOW, &oldt);
	fcntl(STDIN_FILENO, F_SETFL, oldf);	
}

int check_mdex(char *file);

int main (int argc, char **argv)
{
	char *objfile = "a.out";
	char *disk1nam, *disk2nam, *disk3nam;
	unsigned short base, p;
	int i;

#ifdef HOST
	/* when running in host mode, set the base dir path */
	path = strrchr(argv[0],'/');
	if (path) {
		path[1] = 0;
		path = argv[0];
	}
	else {
		path = "./";
	}
	//printf("path=%lx, %s\n", path, path);
#endif

	argc--;
	argv++;
	if (argc>0 && argv[0][0]=='-' && argv[0][1]=='t') {
		traceenable = TRUE;
		argc--;
		argv++;
	}
	if (argc>0 && argv[0][0]=='-' && argv[0][1]=='p') {
		ptraceenable = TRUE;
		argc--;
		argv++;
	}
	if (argc>0) {
		objfile = argv[0];
	}

	clearall ();

#ifdef CORTEX
	/* When running in cortex mode, open the dist images. The default
	 * image names depend on whether the MDEX OS was loaded or not.
	 * Specifiying a disk as "." uses the default name in that position
	 */
	if (check_mdex(objfile)) {
		disk1nam = (argc>1 && argv[1] && (argv[1][0]!='.' || argv[1][1]!=0)) ? argv[1] : "mdex1.dsk";
		disk2nam = (argc>2 && argv[2] && (argv[2][0]!='.' || argv[2][1]!=0)) ? argv[2] : "mdex2.dsk";
		mdexmode = 1;
	} else {
		disk1nam = (argc>1 && argv[1] && (argv[1][0]!='.' || argv[1][1]!=0)) ? argv[1] : "noshd.dsk";
		disk2nam = (argc>2 && argv[2] && (argv[2][0]!='.' || argv[2][1]!=0)) ? argv[2] : "nosfd.dsk";
		disk3nam = (argc>3 && argv[3] && (argv[3][0]!='.' || argv[3][1]!=0)) ? argv[3] : "newhd.dsk";
		mdexmode = 0;
	}
	cf_dsk0 = open(disk1nam, O_RDWR);
	if (cf_dsk0==-1) {
		cf_dsk0 = 0;
	}
	cf_dsk1 = open(disk2nam, O_RDWR);
	if (cf_dsk1==-1) {
		cf_dsk1 = 0;
	}
	cf_dsk2 = open(disk3nam, O_RDWR);
	if (cf_dsk2==-1) {
		cf_dsk2 = 0;
	}
	if (mdexmode)
		printf("dsk0=%d (%s), dsk1=%d (%s)\n", cf_dsk0, disk1nam, cf_dsk1, disk2nam);
	else
		printf("dsk0=%d (%s), dsk1=%d (%s), dsk2=%d (%s)\n", cf_dsk0, disk1nam, cf_dsk1, disk2nam, cf_dsk2, disk3nam);
	
	if (cf_dsk0 == 0) printf("WARNING: running without default disk!\n");
#endif

#ifdef UNIX

	usbsiofd = open("/dev/cu.usbserial-A50285BI", O_RDWR);
	//usbsiofd = open("/dev/cu.SLAB_USBtoUART", O_RDWR);
	if (usbsiofd==-1) {
		usbsiofd = 0;
                printf("No network serial!\n");
	}
#endif

	tracelim[0] = 0;
	tracelim[1] = 0xFFFE;
	run = 1;
	
	tracefd = stderr;

	if (objfile) {
		if (binloader (objfile, 0) != 0) 
			return -1;

#ifdef HOST
		/* When running host emulation, set up argc and argv at
		 * location 0xf800
		 */
		base = 0xf800;
		argc--; argv++;
		while(argc--) {
			memory[base++] = ' ';
			strcpy((char*)memory+base, *argv);
			base += strlen(*argv);
			argv++;
		}
		memory[base++] = 0;
		//printf("args = %s\n", (char*)(memory + 0xf800));
		if (base<0xf800) {
			fprintf(stderr,"argv overrun\n");
			exit(1);
		}
#endif
		tcgetattr(STDIN_FILENO, &oldt);
		atexit(&treset);
#ifdef CORTEX
		/* During emulation run in raw-like mode. For host mode
		 * use host cooked lines (unless the program specifies 'raw').
		 */
		mkrare();
#endif
		runprog ();
	}
	return 0;
}
