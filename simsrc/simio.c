/***********************************************************************
*
* simio.c - IO processing for the TI 990 Simulator.
*
* Changes:
*   05/29/03   DGP   Original.
*   06/20/03   DGP   Added interrupt support.
*   07/01/03   DGP   Fixed floppy unit on STCR of status.
*   07/08/03   DGP   Changed FLOPPY to return status if unit not present.
*   07/09/03   DGP   Forced PC and WP to be on even boundry for interrupts.
*   07/15/03   DGP   Added Card Reader support.
*   07/23/03   DGP   Added ASCII/EBCDIC support for IBM OS/390.
*   08/07/03   DGP   Fixed PANEL support such that selftest and load works.
*   08/12/03   DGP   Added gen/clr/chk interrupt routine and /10 priv checks.
*   11/06/03   DGP   Put Floppy stuff in simfpy.c
*   11/07/03   DGP   Added VDT911 support.
*   11/12/03   DGP   Added TILINE support.
*   12/02/03   DGP   Improved card reader support.
*   05/10/04   DGP   Added DEV_NULL support.
*   07/29/04   DGP   Added devreset function.
*   12/03/04   DGP   Added CRU address adjustment.
*   04/01/05   DGP   Changed IDLE wait time.
*   04/12/05   DGP   Changed CONOUT to use prtview in panel mode.
*   04/13/05   DGP   Added device switches.
*   04/27/05   DGP   Modulo 4 device names for TILINE devices.
*   09/21/05   DGP   Added SWLOGOUT switch.
*   12/04/05   DGP   Added upcase switch on CONIN.
*   08/17/06   DGP   Added mutex to intrequest set/clear.
*   09/28/07   DGP   Dynamically adjust sleeptime for the interval timer.
*   10/01/07   DGP   Use Real Time clock.
*   10/23/07   DGP   Added WIN32 mutex.
*   10/25/07   DGP   Try to improve WIN32 sleep ganularity.
*   11/27/07   DGP   Ensure that the interrupt in attachit is valid.
*
***********************************************************************/

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <memory.h>
#include <ctype.h>
#include <errno.h>
#include <time.h>

#if defined (WIN32)
#define __TTYROUTINES 0
#include <conio.h>
#include <windows.h>
#include <signal.h>
#include <process.h>
#ifndef HZ
#define HZ 100
#endif
#endif

#if 1  || defined(UNIX)
#include <unistd.h>
#include <sys/time.h>
#include <sys/param.h>
#include <sys/ioctl.h>
#include <termios.h>
#include <pthread.h>
#endif

#include "simdef.h"

extern uint16 pcreg;	/* The program PC */
extern uint16 statreg;	/* The program status register */
extern uint16 wpreg;	/* The program Workspace Pointer */
extern uint16 lights;	/* The panel lights */

extern int run;
extern int idle;
extern int runled;
extern int idleled;
extern int faultled;
extern int devcnt;
extern int model;
extern int mapenabled;
extern int intsenabled;
extern int pancount;
extern int enablepanel;
extern int viewlen;
extern int prtviewlen;
extern int prtviewcol;
extern int usertclock;
extern long delayclock;
extern unsigned long instcount;
extern unsigned long mipcount;
extern char view[MAXVIEW][81];
extern char prtview[MAXVIEW][81];

extern uint16 errcrudata;
extern uint16 mapcrudata;
extern uint16 curmapindex;
extern uint32 maplatch;
extern uint8 memory[MEMSIZE];
extern MapFile mapfile[MAPSIZE];

extern uint32 tracemem[16];
extern uint32 errtrace[16];
extern int errindex;
extern int trcindex;
extern int trclatch;
extern int traceenable;
extern int tracestart;
extern int traceend;
extern int tracelim[2];
extern int traceit;
extern FILE *tracefd;

static int clockactive = FALSE;
static int clockthreadactive = FALSE;
uint16 intrequest = 0;
static int sim_int_char = 005;
#ifdef UNIX
static pthread_mutex_t genint_mutex = PTHREAD_MUTEX_INITIALIZER;
#endif
#ifdef WIN32
static HANDLE mhandle;
#endif


/***********************************************************************
* geninterrupt - Generate interrupt
***********************************************************************/

void
geninterrupt (uint16 lvl)
{
   intrequest = intrequest | (1 << lvl);
}

/***********************************************************************
* clrinterrupt - Clear interrupt
***********************************************************************/

void
clrinterrupt (uint16 lvl)
{
   intrequest = intrequest & ~(1 << lvl);
}

/***********************************************************************
* chkinterrupt - Check interrupt
***********************************************************************/

static int
chkinterrupt (uint16 lvl)
{
   uint16 i;

   if (lvl > 0 && !intsenabled) return (0);
   i = intrequest & (1 << lvl);
   return (i);
}


/***********************************************************************
* checkcru - Check cru address and adjust.
***********************************************************************/

static int checkcru (uint16 *pdevaddr, int8 *pdisp)
{
   uint16 devaddr = *pdevaddr;
   int8 disp = *pdisp;

   /*
   ** Adjust CRU address
   */

   if (disp > 15)
   {
      while (disp > 15)
      {
	 disp -= 16;
	 devaddr += 0x20;
      }
   }
   else if (disp < 0)
   {
      while (disp < 0)
      {
	 disp += 16;
	 devaddr -= 0x20;
      }
   }

   *pdevaddr = devaddr;
   *pdisp = disp;
   return (0);
}

struct sio {
   int xintflag;
   int xinttime;
   int rintflag;
   int ttyinput;
   int crubits[32];
};
struct sio aca[2];

#define RBINT	 16
#define XBINT	 17
#define RIENB	 18
#define XIENB	 19
#define RBRL     21
#define XBRE	 22
#define INT      31

/***********************************************************************
* setbitone - Set CRU bit to one.
***********************************************************************/

void setbitone (uint16 inst)
{
   uint16 devaddr;
   int8 disp;
   struct sio *p = NULL;

   disp = inst & 0x00FF;
   devaddr = R12;

//   if( devaddr==0x0080 )
//        printf("SBO %4x,%2d\n", devaddr, disp);

   /*
   ** Check CRU address
   */

   if (checkcru (&devaddr, &disp) < 0)
   {
      return;
   }

   /* 9902 */
   switch(devaddr) {
        case 0x20: p = &aca[0]; break;
        case 0xa0: p = &aca[1];
   }

   if (p) {
	switch (disp) {

	case XIENB-16:
	p->crubits[XIENB] = 1;
	p->xintflag = 0;
	break;

	case RIENB-16:
	p->crubits[RIENB] = 1;
	p->rintflag = 0;
	break;
	}
   }
   /* mapper */
   if ((devaddr==0x40 & disp==1)) {
	mapenabled = TRUE;
	//printf("map ON\n");
   }
   return;
}

/***********************************************************************
* setbitzero - Set CRU bit to zero.
***********************************************************************/

void setbitzero (uint16 inst)
{
   uint16 devaddr;
   int8 disp;
   struct sio *p = NULL;

   disp = inst & 0x00FF;
   devaddr = R12;

// printf("SBZ %4x,%2d\n", devaddr, disp);

   /*
   ** Check CRU address
   */

   if (checkcru (&devaddr, &disp) < 0)
   {
      return;
   }

   switch(devaddr) {
        case 0x20: p = &aca[0]; break;
        case 0xa0: p = &aca[1];
   }
   if (p) {
		switch (disp) {

		case XIENB-16:
		p->crubits[XIENB] = 0;
		p->xintflag = 0;
		break;

		case RIENB-16:
		p->crubits[RIENB] = 0;
		p->rintflag = 0;
		break;
	}
   }
   /* mapper */
   if ((devaddr==0x40 & disp==1)) {
	mapenabled = FALSE;
	//printf("map OFF\n");
   }
   return;
}

/***********************************************************************
* testbit - Test CRU bit.
***********************************************************************/

void testbit (uint16 inst)
{
   uint16 devaddr;
   int8 disp;
   uint8 c;
   struct sio *p = NULL;

   disp = inst & 0x00FF;
   devaddr = R12;

   /*
   ** Check CRU address
   */

   if (checkcru (&devaddr, &disp) < 0)
   {
      return;
   }

//   printf("TB  %4x,%2d\n", devaddr, disp);
   
   switch(devaddr) {
        case 0x20: p = &aca[0]; break;
        case 0xa0: p = &aca[1];
   }
   if (p) {
		switch (disp) {
		case RBRL-16:
		if (p->ttyinput>=0) {
			SET_EQ;
			return;
		}
		break;

		case XBRE-16:
		if (p->crubits[XBRE]) {
			SET_EQ;
			return;
		}
		break;

		case XIENB-16:
		if (p->crubits[XIENB]) {
			SET_EQ;
			return;
		}
		break;

		case XBINT-16:
		if (p->xintflag) {
			SET_EQ;
			return;
		}
		break;

		case RBINT-16:
		if (p->rintflag) {
			SET_EQ;
			return;
		}
		break;
		
		case INT-16:
		if (p->rintflag || p->xintflag) {
			SET_EQ;
			return;
		}
		break;
	}
	CLR_EQ;
	return;
   }

   CLR_EQ;
   return;
}

/***********************************************************************
* loadcru - Load CRU.
***********************************************************************/

static int stime;
static char c[2];
extern int usbsiofd;

void loadcru (uint16 inst, uint16 dat)
{
   uint16 reg;
   uint16 tag;
   uint16 cnt;
   uint16 devaddr;
   struct sio *p = NULL;

   reg = inst & 0x000F;
   tag = (inst & 0x0030) >> 4;
   cnt = (inst & 0x03C0) >> 6;

   devaddr = R12;

   /*
   ** Check for privleged CRU address
   */
   switch(devaddr) {
        case 0x00: p = &aca[0]; break;
        case 0x80: p = &aca[1];
   }

   if( p && cnt==8 ) {
//	   if (dat=='\r') dat = '\n';
// printf("LDCR %d=%c\n", dat, dat);
           if( devaddr==0x00 ) {
		char c = dat&0x7f;
		putchar(c);
		fflush(stdout);
           } else {
               c[0] = dat;
               if(usbsiofd)
                        write(usbsiofd, &c, 1);
               else
                        printf("No serial connection! %x\n", devaddr);
           }
	   p->crubits[XBRE] = 1;
	   p->xintflag = 0;
	   p->xinttime = stime;
   }

   return;
}

/***********************************************************************
* storecru - Store CRU.
***********************************************************************/

uint16 storecru (uint16 inst)
{
   uint16 reg;
   uint16 tag;
   uint16 cnt;
   uint16 devaddr;
   uint16 dat;
   uint8  c;
   struct sio *p = NULL;

   reg = inst & 0x000F;
   tag = (inst & 0x0030) >> 4;
   cnt = (inst & 0x03C0) >> 6;

   dat = 0;
   devaddr = R12;

   /*
   ** Check for privleged CRU address
   */
   switch(devaddr) {
        case 0x00: p = &aca[0]; break;
        case 0x80: p = &aca[1];
   }

   if( p && cnt==8 ) {
	if (p->ttyinput>=0) {
		dat = p->ttyinput;
		p->ttyinput = -1;
		p->rintflag = 0;
	}
	if (dat=='\n') dat = '\r';
	if (dat==0177) dat = '\b'; /* DEL to BS */
	if (dat==3) {
		run = 0;    /* ^C is stop runing */
		printf("\n");
	}
   }

   return (dat);
}

/***********************************************************************
* checkinterrupts - If any pending and not masked, take it.
***********************************************************************/

void 
checkinterrupts (void)
{
	int curlvl;
	int i,diff;
	static int usec;
	uint16 curinst;
	struct timeval tv;
	unsigned char c;
        struct sio *p;

#ifdef HOST
	/* no need for the interrupt routine when running host emulation */
	if (!GET_MASK) return;
#endif

	do {
		curinst = GETINST;
    
		/* maintain time */
		gettimeofday(&tv, NULL);
		stime = (int)tv.tv_usec;

                /* handle I/O on the serial line * /
                p = &aca[1];
                if (usbsiofd && p->xintflag==0) {
                        diff = stime - p->xinttime;
                        if (diff<0 || diff>2042) { // approx. 9600 baud for 7E1
                                // check for tty output
                                p->xintflag = 1;
                        }
                }
                if (p->xintflag && p->crubits[XIENB]) {
                        geninterrupt(1);
                }
                if (usbsiofd && p->ttyinput==-1 && p->crubits[RIENB] && read(usbsiofd, &c, 1)==1) {
                        p->ttyinput = c;
                        p->rintflag = 1;
                }
                if (p->rintflag && p->crubits[RIENB]) {
                        geninterrupt(1);
                } */
                
		/* tty0 xmit interrupts */
                p = &aca[0];
                if (p->xintflag==0) {
                        diff = stime - p->xinttime;
                        if (diff<0 || diff>2042) { // approx. 9600 baud for 7E1
                                // check for tty output
                                p->xintflag = 1;
                        }
                }
                if (p->xintflag && p->crubits[XIENB]) {
                        geninterrupt(4);
                }

		/* timer interrupt 60 HZ */
		diff = stime - usec;
		if (diff<0 || diff>16666) {
			usec = stime;
 			// check for tty0 input 60 times per second as well
                        p = &aca[0];
			if (p->ttyinput==-1 && read(STDIN_FILENO, &c, 1)==1) {
				p->ttyinput = c;
				p->rintflag = 1;
				if (p->crubits[RIENB]) {
					geninterrupt(4);
				}
			}
			// clock interrupt
			geninterrupt(3);
		}

		/* process interrupt */
		if (intrequest)
		{
/*for(i=0;i<8;i++) {
	printf("loc[%d]=%x\n",i,GETMEM0(i*2));
}*/
			curlvl = GET_MASK;
			for (i = 0; i <= curlvl; i++)
			{
				if (chkinterrupt (i))
				{
					uint16 opc, npc;
					uint16 owp, nwp;
					uint16 ost;
if (i==0) printf("int 0!\n");

					if (curinst == IDLEINST) pcreg += 2;
					ost = statreg;
					opc = pcreg;
					owp = wpreg;
					clrinterrupt(i);
					SET_MASK (i-1);
					CLR_NONPRV;
					CLR_MAP;
					CLR_XOP;
					CLR_OVERI;
					CLR_WCS;
					nwp = GETMEM0 (i * 4) & 0xFFFE;
					npc = GETMEM0 (i * 4 + 2) & 0xFFFE;
//					nwp = 0xF0BE;
//					npc = 0xF0DE;
					pcreg = npc;
					wpreg = nwp;
					PUTREG (13, owp);
					PUTREG (14, opc);
					PUTREG (15, ost);
					idle = FALSE;
					return;
				}
			}
		}
		if (idle) usleep(1000);
	} while (idle);
}

void init_acas()
{
        struct sio *p;
        int i;
        
        for(p = &aca[0], i = 0; i<2; p++, i++) {
                p->crubits[XBRE] = 1;
                p->xintflag = 1;
                p->ttyinput = -1;
        }
}
