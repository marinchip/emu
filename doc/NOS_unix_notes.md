## NOS/MT versus early Unix, a compare and contrast

### Introduction

Between 1978 and 1981, John Walker's company Marinchip Systems worked on the S-100 bus based M9900 computer and its companion operating system **NOS/MT**. The M9900 was based on the Texas Instruments 9900 CPU chip, which architecturally was quite similar to the PDP11, at least as far as the instruction set architecture is concerned. John writes:

*The goal was to sell a “real” computer: a PDP-11 class machine with a rational architecture, 16 bit arithmetic, multiply and divide, and good context switching, which would be compatible with S-100 memory, peripherals, and enclosures, all of which were rapidly advancing and falling in price. This was to be a platform supporting a Unix-like operating system with usable systems- and application programming languages. The object was to deliver a personal computing platform which provided power comparable to a timesharing terminal connected to a mainframe or minicmputer running Unix. Marinchip would make its money selling the S-100 boards which enabled the system. The basic software would be bundled with the boards; since it couldn't run on any other hardware, software piracy would not be a problem.*

As part of this effort, Marinchip - along with a few other one-man companies around it - produced an operating system and software stack that delivered a lot of that ambition. In some ways the result closely resembles Unix, in other ways it regresses and in yet other ways it advances on Unix as it stood at the time.

One important aspect is that NOS/MT aimed to be a network operating system, meaning that various nodes could be connected into a coherent local network. This aspect was never fully developed, but several of its design choices are driven by this aspiration. John writes:

*The original concept was what Sun Microsystems later called “The Network is the Computer” (this was, of course, years before Sun was founded in 1982). The overall concept was similar to what Apple introduced in 1985 as AppleTalk for the Macintosh and Lisa. I never got further in development of the network features than demonstrating the synchronous data link between two M9900 systems in different rooms.*

As it turned out, hardware was moving ahead and the 9900 chip family lost its competitive edge. Marinchip evolved into Autodesk and succeeded beyond expectation with AutoCAD. In John's words:

*Because Autodesk has been successful, there's a tendency to forget what a high-risk undertaking it was to start the company. Most of the founders of Autodesk were involved in preexisting ventures of their own, some while also holding down full-time jobs. Embarking on Autodesk meant abandoning these ventures, some of which looked quite promising at the time, for a new business in an untested market. Starting Autodesk wasn't the only opportunity that beckoned at the time. I wrote [a strawman] paper in late 1981 to plot the strategy of Marinchip and the people around it, who encompassed a large percentage of the Autodesk founders. This strategy represented the “safe evolutionary path” for Marinchip and would, had it been pursued, have led to utter failure.*

### 1. Choice of language

A first interesting difference is the implementation language. Whereas Unix is implemented in (an early form of) C, the NOS/MT kernel and most of its core utilities are written in assembler. 

It is not entirely clear why this choice was made. There are two obvious possibilities:

1. Marinchip did not have a suitable compiler in hand. For sure John Walker looked into compilers early on, with the META compiler generator and the Birnch Hensen Pascal compiler (used to implement the "Solo" PDP-11 operating system) being among the first programs brought to the nascent OS. It seems that the conclusion was that these options did not result in compilers that were practical for OS development (slow, modularity, etc.)
2. The 9900 chip delivered less power than a mid-range PDP-11. Maybe the conclusion was that in order to be competitive on performance, the OS could not afford the approximately 30% performance loss that Ritchie had estimated for using C.

A contributing factor may have been that John was a very accomplished assembly language programmer. Churning out large amounts of assembler code would not have daunted him.

In the end this choice also doomed NOS/MT: when the 9900 range of CPU's was no longer competitive, the code base could not be brought to newer options with minimal porting effort. This portability may have been Unix' strongest point in the early 80's.

Seeing Marinchip's struggles in finding a good compiler puts into focus the achievement of the early C compiler. Whereas compiling logic flow statements is relatively straightforward, compiling expressions and assignments into efficient assembler code on a small machine was a breakthrough achievement.

### 2. System calls

#### 2.1 Unix-like calls

Like Unix, NOS/MT has a small set of system calls. Most would appear very familiar to early Unix users:

|             | System call table |             |
|:------------|:----------------|:--------------|
|exit         | [fork]          | exec          |
|open         | close           | create        |
|read         | write           | seek          |
|link         | delete (unlink) | mkdir (mknod) |
|fstat (stat) | ostat (fstat)   | sync          |
|fdup         | asdir (chdir)   | delay (sleep) |
|mem (sbreak) | ioctl           | trap (signal) |
|mount        | dmount (umount) | chacc (chmod) |
|chown        | getid           | setid         |
|time         | stime           |               |

Most of these calls operate just like their Unix equivalents. Some are a bit different:

  * There is no fork system call exposed to the user, although all the infrastructure is there and fork does exist internal to the kernel. NOS/MT has a very different process model and in modern terms the NOS/MT fork system call would have a created a new thread in the current process rather than a new process. 
  * The mkdir call is not a direct equivalent of mknod. It creates a new directory, including the . and .. entries.
  * The mem system call just reports available space. There is no system notion of a "break", nor of a stack segment; these are managed by the user program.
  * Although trap is equivalent to signal, there is no equivalent to the "kill" system call. Only the system can generate signals, and only the SIGINT (^C) equivalent is implemented.
  * The calls mount and dmount mount disk volumes, but not on a directory. Instead they mount on separate mount points called "storage units". These are referenced by number ("1:/", etc.) and once mounted also by volume name (e.g. "NOS:/"

Some of the above differences will become more clear and logical in the below discussion of NOS/MT system organization.

#### 2.2 NOS/MT specific calls

There are also 4 system calls that are specific to NOS/MT and are not modelled on a Unix equivalent:

| Call      | Purpose |
|:----------|:--------|
| alloc     | create a "contiguous" file
| flock     | lock access to a file
| vstat     | report on mounted volumes
| script    | start processing a script file
| prep      | create an empty file system on a 'raw' disk
| sboot     | set kernel image file to boot from

The system call to lock files was used to access database files. They are advisory locks on the whole file. They were of practical use in the "Osborne" accounting package that was sold for NOS/MT and the M9900 system.

NOS/MT does not have a shell in the Unix sense and "script" is a system that redirects input for the shell equivalent to read from a file instead.

The "prep" system call is effectively the same as the "mkfs" command on early Unix. It is unclear why this was turned into a system call instead of a utility program.

NOS/MT was designed to work with the simple boot PROM of M9900 systems. The kernel was always stored as a contiguous file and its location was recorded in the header block of the volume. The made it easy for the boot PROM to load the kernel.

#### 2.3 Call mechanism

Part of the elegance of early Unix is it call interface with a single return and at most 3 parameters. In early Unix, this is more evident at the C language level than at the assembler level.

In NOS/MT the assembler level interface is organised around a "request packet", a data structure with input and output parameters. This is similar to the *supervisor control blocks* used by other contemporary operating systems. The structure of these packets is described in detail in the NOS/MT user manual. Not having the call parameters in registers means that the call parameters must be marshalled across the user space / kernel space divide.

None of the higher level languages available for NOS/MT (Basic, Pascal and SPL) made the system calls available as simple function calls. That said, both the Basic and the SPL compiler can link to functions in an external library. It would certainly have been possible to provide the equivalents of the standard Unix system call functions. Apparently, this was not seen as useful at the time.

### 3. File system

The file system of NOS/MT is at the same time very similar and very different to the V6 or V7 Unix file system. The below comments from John explain why:

*The file system was explicitly designed to be Unix-like, as you can see from the definitions in the [header] file.  However, I at the time I had no access to Unix source code or even to a running Unix system, so the design was based entirely on the Bell System Technical Journal articles' description of the file system, hence it's unlikely the exact structure would be the same. I don't recall ever hearing the word "superblock" until I started to work with Sun Microsystems workstations in the mid-1980s.*
 
*The NOS/MT file system was originally developed on a floppy disc drive. The port to the first hard drive was done over a weekend, so it's unlikely there's much difference between the file system structure on a floppy and hard drive.  Doubtless details such as block size and the size of allocation tables were adjusted to the physical parameters of the drive, but I'm pretty sure the basic structure was the same.*

The great similarities are the use of inodes (with a structure almost the same as early Unix) and directories being files with 16 byte entries: a 14 byte name and 2 bytes for the inode number. The main difference is that free blocks are not maintained in a free list, but in a bitmap (somewhat like Unix would do in 9th edition a few years later).

The NOS/MT does not have te notion of a "superblock" in the Unix sense. There is a header block with disk information (volume name, geometry, etc.) but it does not contain a cache of free blocks and inodes.

Like very early Unix, files could be stored in one of three formats: contiguous, regular and large. Contiguous files are - as the name suggests - contiguous blocks on disk and the inode holds the first block number and the number of blocks. Normal files have the block numbers in the inode (no indirection). Large files work with indirection.

Block numbers are 16 bit, so disks are limited to about 16MB on disks with 256 byte sectors and about 32MB on disks with 512 byte sectors, similar to Unix V6.

The internal structure of the filesystem code is similar to that of Unix: there is a block cache layer, an inode layer, a file table and a path walker. In a difference to Unix, the NOS/MT file system code includes a disk formatting routine as a kernel service.

### 4. Device special files

Like Unix, NOS/MT has device special files. However, they are implemented in a way that makes them much less flexible. Probably this was technical debt from MDEX that was not yet corrected when the Marinchip team was pivoting into AutoCAD.

The "bootstrap OS" for NOS/MT was **"MDEX"**, a minimal executive of just a few kilobytes in size. Interestingly, MDEX implemented a subset of the system call set of the envisioned NOS/MT in a forward compatible way. Hence, programs developed under MDEX would be able to be used unchanged on NOS/MT (and they did).

MDEX used a very simple filesystem and did not have an inode equivalent. Special files were hardcoded into the OS by name and the *open* system call would check this table to see if a special file was openened. "CONS.DEV" was the system console, "PRINT.DEV" the printer, "DISC1.DEV" provided raw access to the first (floppy) disk, etc. An interesting special file is "PARAM.DEV", which returns the command parameter line when read.

As a quick hack, the special files on MDEX opened with fixed file handle ("file descriptor" in Unix parlance) numbers: for example, CONS.DEV was always 0, and PARAM.DEV was always 255. The application programs relied on this, often accessing these file handles without even opening the file -- and MDEX accepted this. Hence, NOS/MT had to support this behaviour.

The result is that even though NOS/MT has all the infrastructure to do device special files the Unix way (it has the inode flags defined, internally it has driver switch tables similar to Unix, etc.), it actually does not use that. Instead it implements a scheme with hardcoded device names and fixed file handles. These are defined as part of the system confiuration.

Had NOS/MT development not stopped when it did, probably this would have been fixed. One step was already set: in NOS/MT the console is not only opened on descriptor 0, but on all of 0, 1 and 2 -- stdin, stdout and stderr. However, all MDEX era programs use file handle 0 for both console input and console output.

### 5. Memory management

NOS/MT has a very different memory model, driven by very different hardware capabilities as compared to the PDP-11. This in turn also drove a different process model. John writes:

*Since the 9900 had no supervisor or user mode, the only distinction between kernel space and user space was which memory bank was selected.  One bank held the system and there was one bank for each user.*

Beyond not having a supervisor mode (in a way, the 9900 was to the TI990 mini, what the T11 microprocessor was to the PDP-11), the boards that Marinchip designed around the 9900 chip had very limited MMU capabilities, essentially just memory banking. Because of this a swapping architecture like early Unix made little sense. With a simple mapper, a "scatter paging" design (like Unix 32V) would have been possible, but the fact remains that this hardware did not exist at the time.

Because of this, NOS/MT has a design based around a fixed number of processes, one for each memory board. Somewhat confusingly, NOS/MT refers to a process as a "user (space)". This concept is distinct from the user id. Each user space is also associated with a terminal, analogous to the "controlling terminal" in early Unix.

In order for a real user to run multiple processes at once the concept of a "batch" or background process was created. These were memory boards / processes that did not have a fixed terminal associated, but used a pseudo terminal that could be opened by 'real' users. Opening such a pseudo terminal was the equivalent of forking a sub-process, inheriting the user id and the default directory. It did not inherit the open file table, and in that sense it was more like "spawn" than "fork" in concept.

In order for the system to communicate, a 4KB block of shared memory was available at all time. This block of memory held the system call interface and the interrupt interface to the full kernel. The kernel ran in its own memory bank, separate from the user and batch memory banks in the system.

### 6. A micro-kernel?

In the shared area of memory, there is something that can arguably best be described as a message passing micro-kernel. Each system call is changed into a message packet that is sent to other - conceptually possibly remote - parts of the system. The kernel as a whole was a monolithic program, running in a single system address space; it is multithreaded though, with a kernel thread for each device driver, for the file system, for each user space, etc. Basically, each such service sits in a loop waiting for messages to arrive and process.

John summarised this concept as follows:

*I wouldn't necessarily call it a message passing system, although many parts of NOS/MT worked by passing packets through bounded buffers between co-operating processes (what are now called "threads").  The kernel of the system was based upon the design of Edsger W. Dijkstra's THE Multiprogramming System, with a simple scheduler that provided primitives to create and terminate threads, synchronise them with semaphores, manage doubly-linked lists of buffers, and provide bounded buffers built from these primitives.  (A bounded buffer is a one-way communication channel between two threads where the receiver blocks when there is no buffer for it and the sender blocks when a specified number of packets are waiting in the buffer.)*

*The register pointer architecture of the 9900 made process switching very fast, as you only needed to change the pointer to the registers of the new process.  All of the services within the system (file system, device drivers, etc.) were threads which communicated via bounded buffers.  This architecture was intended to support a system where the services were located on different machines and communicated over a network, but this was never implemented.*

 *The upper memory, which was always mapped regardless of which bank was selected, contained the interrupt handler which allowed switching banks when an interrupt occurred and the fundamental scheduler primitives (what you describe as the microkernel).  I believe these services were available to user programs as well as the system (i.e. user programs could create threads), but at this remove I can't recall whether that was actually implemented in the final version of NOS/MT before I stopped working on it.  It certainly was intended in the design.  However, none of the Marinchip applications used these features, since doing so would have made them incompatible with MDEX, and that accounted for most of our users.*

Since John offered those memories, the manual for a 1982 version of NOS/MT was found and its source code recreated. With these, the conclusion is that indeed user processes were still blocked from creating new threads, this was only allowed at the system level.

### 7. Processes, threads and users

The NOS/MT process model is quite different from Unix, already starting with the nomenclature used.

In ancient Unix, a user process is in simple terms the combination of private memory, a thread of execution and an associated user / terminal. Memory allocation is very dynamic, with a process  potentially moving around in memory during its lifetime (this of course ignores nuances that read-only section of memory can be shared, that each execution thread has a user and a kernel part and that some processes have no associated user terminal).

A Unix **fork** system call allocates new memory (with contents copied from its parent), a new thread of execution, with this new process sharing the user terminal with its parent.

In NOS/MT things are organised around users / terminals, including virtual users / terminals ("batch"). Each user /terminal has a fixed, private memory allocation and one or more user threads of execution. There is also one system thread per user that manages the user terminal and is the recipient of all system calls from the user's threads.

A NOS/MT **fork** system call does not allocate new memory, but only creates a new thread of execution. Memory and the user / terminal are shared between all threads of this user. As John describes above, the internal fork system call was not exposed to user programs in the released versions of NOS/MT.

For a modern reader, the NOS/MT term "user" should be read as "process", and the NOS/MT term "process" should be read as "thread". NOS/MT reflected its hardware environment in that it had a fixed number of processes, with one process per memory board. In principle there was one physical terminal per process, but there could be processes (i.e. memory boards) that had a pseudo terminal which could be claimed by other processes as needed. These were called "batch" processes.

Taking this idea one step further, Marinchip also developed (but never marketed) things called "node boards", which had a CPU and private memory. These were accessed in the same way as batch processes, communicating via a block of shared memory.

As we now know, the market shifted to (networked) personal computers instead.

### 8. The shell

NOS/MT has a shell program, **SH**, but this is a very different thing as compared to the Unix shell. In modern terms, the NOS/MT shell is much like **busybox**: a single program that combines many utility functions and changes its function based on the name by which it is called.

The shell function is provided by two parts working together. At the top level there is a minimal shell consisting of a line editor and a minimal command processor, both part of the kernel. The line editor is similar to the line editing functions provided by the 'cooked mode' tty driver in early Unix. The command processor implements a simple loop that reads lines and parses these as a command with parameter words. It has only two internal commands: mount and dismount. All other commands are external commands that are looked up in the current directory and if not found in the "1:bin" directory (i.e. similar to V6 Unix).

The big difference comes when a command is loaded and run. The command processor forks a new thread and this new thread starts executing the program. The command processor thread leaves the command line loop and enters a message loop. When the user program makes a system call, the request packet is sent to this command message loop where it is processed or dispatched. System calls related to files are dispatched to the filesystem service thread, etc. Receipt of an "exit" system call packet makes the command processor leave its message loop and return to its command line loop.

The main shortcomming of the implementation is that the command line processor is a bit too simple with no syntax for input / output redirection, and no support for 'globbing'. These could have been added, but it is complicated by application programs using file handle zero for both input and output, instead of splitting this over two file handles.

The above also implies that a different mechanism for running scripts is needed. The command loop implements a system call *script* that it handles itself: it opens the file as input and recursively enters the command loop on this file. The command loop opens each external program anyway (to check if it is a native, pascal or basic binary) and could have checked for a 'hash-bang' type marker to recognise a script file automatically. However, it does not do this.

### 9. Command line sytax and globbing

As already mentioned the command line of NOS/MT is very simple, it is parsed into a command and a list of arguments.

NOS/MT supports a list of arguments that is space separated, but its native argument format is different, also using the equals sign and comma as separators. A NOS/MT command line could look like:

```
asm hello.rel=hello.asm,list.txt
```

These arguments would be understood as ' hello.rel', '=hello.asm' and ',list.txt' respectively.

There is no equivalent for the operators **>**, **<**, **|** and **&**. When using NOS/MT versus early Unix, they are missed. Other than the mixing of input and output over file handle 0 by MDEX era programs, it would not have been hard to add.

The most inconvient absence on the command line, however, is globbing (i.e. expanding parameters containg * and ? into file names). This would have been a bit harder to add, and the fact that a NOS/MT command line is limited to 80 characters (versus on early Unix 512 characters) does not help.

The only command to support globbing on NOS/MT is the **mcopy** command, to copy multiple files to a new directory. It would have made sense in the **deldir** command, but there it doesn't exist.

### 10. Pipes and IPC

The **pipe** system call is notably absent from NOS/MT. Due to its different process model it makes less sense to have this feature. However, other mechanisms exist.

First of all, there is the multi-threaded nature of NOS/MT processes. Communication between threads can work via shared memory and semaphores. This is how the kernel itself is implemented.

Next there is the "batch" pseudo terminal. This works quite similar to a pseudo-terminal pair in early Unix, making it similar to a two-way pipe with limited storage (depending on configuration, but typically about a quarter kilobyte).

The most interesting one is the messaging system used in the kernel. It would have been quite easy to expose the kernel mechanisms to send, receive and test for messages to user programs via one or more new system calls. However, this did not happen -- probably John was hesitant to expose features which would make NOS/MT lose its backwards compatibility with MDEX, similar to his reason for not exposing multi-threading to user programs.

All in all, IPC is an area where NOS/MT could have surpassed early Unix in 1981. That said, by 1982 Unix had its network stack added which included interprocess messaging.

### 11. Standard library

The standard I/O library for NOS/MT is more line oriented than on early Unix. Note in this context that the "standard I/O library" that we know today only arrived with Unix 7th edition (1979). Before that Unix used a simpler library ("iolib") and the NOS/MT facilities are more similar to this earlier version.

The text standard I/O library on NOS/MT consists of three parts. The first two parts are the libraries *textin.rel* and *textout.rel*. These two provide buffered access to files, reading and writing lines of text. The third part is a library *edit.rel*, which is also accessible in the shared memory area of each process.

One can think of *edit.rel* as the equivalent of **printf**, accepting text containing output markers (& for NOS/MT, % on Unix) which are to be replaced by values. In the *edit.rel* library this substitution is less automatic: each element has to placed with a separate library call. In assembler code this is not much harder than building an argument list for a single call, so it fits the  context. The output of *edit.rel* is a formatted line, ready to be output via the *textout.rel* library, or directly if no buffering is required. There is no equivalent **scanf** library on NOS/MT (nor is there on pre-1979 Unix).

Libraries for single and double precision floating point numbers are provided (the 9900 chip did not offer hardware floating point) but there are no libraries for trigonomic, etc. functions.

#### 12. Networking

The last aspect of NOS/MT to discuss is networking, after all it is called NOS/MT which stands for &quot;**Network** Operating System / Multi-Tasking". The code as realised does not implement any networking. However, the kernel structure is set up such that messages can be non-local, i.e. sent from machine to machine. John writes about this:

*"The original concept was what Sun Microsystems later called “The Network is the Computer” (this was, of course, years before Sun was founded in 1982). Using the fast synchronous communication facilities of TI's serial chip [the TMS9901], which was used on the Marinchip Quad SIO board, would allow networking NOS/MT systems and for their file systems to be shared among machines interconnected by these serial links.*

*This would allow, for example, a large and expensive hard disc connected to one machine to serve as file storage for other machines connected to the network. I had spent the early 1970s developing a multi-node communication network for [a company called] Information Systems Design, so I already had all of the communication protocols (which I had designed myself) in hand to build this network.*

*The overall concept was similar to what Apple introduced in 1985 as AppleTalk for the Macintosh and Lisa. I never got further in development of the network features than demonstrating the synchronous data link between two M9900 systems in different rooms. This was envisioned entirely as a local area network, although it would have been possible to connect remote systems over a dial-up link, but at the speeds of modems of the epoch, painfully slow."*


