/***********************************************************************
*
* simdef.h - Simulator header for the TI 990 computer.
*
* Changes:
*   05/29/03   DGP   Original.
*   11/06/03   DGP   Added fpy* and v911* definitions.
*   11/11/03   DGP   Added System environment definitions.
*   07/14/04   DGP   Added more /12 defintions.
*   10/28/04   DGP   Added ECC Device.
*   12/06/04   DGP   Added v945 definitions.
*   03/30/05   DGP   Changed V911SCREENSIZE to 2048.
*   03/13/05   DGP   Bumped up the number of devices permitted.
*   03/28/05   DGP   Moved devices options into switches.
*   09/21/05   DGP   Added SWLOGOUT switch.
*   07/18/07   DGP   Added Hi/Lo intensity support.
*
***********************************************************************/

#include <stdio.h>

/*
** Definitions
*/

#define NORMAL 0
#ifndef ABORT
#define ABORT  12
#endif


#ifndef TRUE
#define TRUE 1
#endif
#ifndef FALSE
#define FALSE 0
#endif

#define VERSION "1.5.7"
#define INTRO "TI 990 Simulator"

/*
** Configuration definitions
*/

#define MEMSIZE 512*1024	/* Maximum size of physical memory */

#define MAXDEVICES 20		/* Number of devices permitted */
#define MAXVIEW 100		/* Number of messages in view panel */

#define TOPLINE       1
#define REGISTERLINE  3
#define DISPLINE      5
#define BREAKLINE     6
#define WORKSPACELINE 8
#define OUTPUTLINE    14

/*
** System definitions.
*/

#define TILINESTART 0xF800
#define TILINEEND   0xFC00
#define ROMSTART    0xFC00
#define TPCSSTART   0x1F0000

#define PRVCRU   0x1C00
#define CRUEND   0x1FFE

#define LOADADDRESS 0xA0

#define EOFSYM ':'
#define HEXSYM '>'

/*
** Default device addresses and interrupt levels.
*/

#define TERMINAL 0x0000		/* Serial Terminal CRU */

#define BKPTCRU  0x1F80		/* Breakpoint CRU */
#define MAPPER   0x1FA0		/* Memory Mapper CRU */
#define ERRCRU   0x1FC0		/* Errors CRU */
#define PANEL    0x1FE0		/* Front Panel CRU */

#define ERRINT   2		/* Error interrupt level */
#define CLOCKINT 5		/* Clock interrupt level */
#define TERMINT  6		/* Serial Terminal interrupt level */

/*#define ECCADDR	 0xFB00*/	/* ECC Memory controller address */

/*
** ERRCRU bits
*/

#define ERRAOVR 0x0010 /* Arithmetic overflow */
#define ERR12MS 0x0020 /* 12MS clock */
#define ERRBKPT 0x0040 /* Breakpoint */
#define ERRSOVR 0x0080 /* Stack Over/Under flow */
#define ERRWRIT 0x0100 /* Write in non-write memory */
#define ERREXEC 0x0200 /* Exec in non-exec memory */
#define ERRMAP  0x0800 /* Memory mapping error */
#define ERRTIP  0x1000 /* TILINE memory parity error */
#define ERRILO  0x2000 /* Illegal operation */
#define ERRPIN  0x4000 /* Priv instruction */
#define ERRTIT  0x8000 /* TILINE timeout */

/*
** Error trace bits
*/

#define TRACE_INSTFETCH	0x00100000
#define TRACE_WSACCESS	0x00200000
#define TRACE_TLRW	0x00400000
#define TRACE_TLACCESS	0x00800000
#define TRACE_WSRW	0x01000000
#define TRACE_PRIVINST	0x02000000
#define TRACE_ILLOP	0x04000000
#define TRACE_MAPERR	0x08000000
#define TRACE_MEMDATA	0x10000000
#define TRACE_TLTIME	0x20000000
#define TRACE_EXECVIO	0x40000000
#define TRACE_WRITVIO	0x80000000

/*
** Object output formats
*/

#define OBJFORMAT "%c%04X"
#define IDTFORMAT "%c%04X%-8.8s"
#define REFFORMAT "%c%4.4X%-6.6s"
#define DEFFORMAT "%c%4.4X%-6.6s"

/*
** Data type definitions
*/

#define int8            signed char
#define int16           short
#define int32           int
typedef int             t_stat;                         /* status */
typedef int             t_bool;                         /* boolean */
typedef unsigned char   uint8;
typedef unsigned short  uint16;
typedef unsigned int    uint32, t_addr;                 /* address */

#if defined (WIN32)                                     /* Windows */
#define t_int64 __int64
#elif defined (__ALPHA) && defined (VMS)                /* Alpha VMS */
#define t_int64 __int64
#elif defined (__ALPHA) && defined (__unix__)           /* Alpha UNIX */
#define t_int64 long
#else                                                   /* default GCC */
#define t_int64 long long
#endif                                                  /* end OS's */
typedef unsigned t_int64        t_uint64, t_value;      /* value */
typedef t_int64                 t_svalue;               /* signed value */

#define MSB 0
#define LSB 1

/*
** Memory
*/

#define MEMLEN		TILINESTART
#define MAPMEMLEN	1024*1024

extern uint8 memory[];		/* The CPU memory */

#define MAPSIZE 3

typedef struct
{
   uint16 l1;
   uint16 b1;
   uint16 l2;
   uint16 b2;
   uint16 l3;
   uint16 b3;
} MapFile;

/*
** Mapper CRU bits
*/

#define MAP_ENABLE 0x0008
#define MAP_RESET  0x0010
#define MAP_LATCH1 0x0080
#define MAP_LATCH2 0x0040
#define MAP_LATCH3 0x0020

/*
** /12 CPU memory protection
*/

#define MEM_NOPROT	0x0
#define MEM_NOWRITE	0x1
#define MEM_NOEXEC	0x2

/*
** Access macros
*/

#define SRC 1
#define DST 0
#define NOLD -1

extern uint16 getinst (void);
extern uint16 getreg (uint16);
extern void putreg (uint16, uint16);
extern uint16 getmem (uint16, int);
extern uint8 getmemb (uint16, int);
extern void putmem (uint16, uint16, int);
extern void putmemb (uint16, uint8, int);

#define GETINST getinst()

#define GETREG(r) getreg(r)
#define PUTREG(r,v) putreg(r,v)

#define GETMEM(m,sd) getmem(m,sd)
#define GETMEMB(m,sd) getmemb(m,sd)

#define PUTMEM(m,v,sd) putmem(m,v,sd)
#define PUTMEMB(m,v,sd) putmemb(m,v,sd)

#define R0  GETREG(0)
#define R1  GETREG(1)
#define R2  GETREG(2)
#define R3  GETREG(3)
#define R4  GETREG(4)
#define R5  GETREG(5)
#define R6  GETREG(6)
#define R7  GETREG(7)
#define R8  GETREG(8)
#define R9  GETREG(9)
#define R10 GETREG(10)
#define R11 GETREG(11)
#define R12 GETREG(12)
#define R13 GETREG(13)
#define R14 GETREG(14)
#define R15 GETREG(15)

#define GETMEM0(m) (((memory[(m)+MSB] & 0xFF) << 8)|(memory[(m)+LSB] & 0xFF))
#define GETMEMB0(m) (memory[(m)] & 0xFF)

#define PUTMEM0(m,v) \
{ memory[(m)+MSB] = (v) >> 8 & 0xFF; memory[(m)+LSB] = (v) & 0xFF; }
#define PUTMEMB0(m,v) memory[(m)] = (v) & 0xFF

#define DISPMEM GETMEM0(dispaddr)

/*
** Status register set and check
*/

#define CLR_LGT    (statreg &= 0x7FFF)
#define CLR_AGT    (statreg &= 0xBFFF)
#define CLR_EQ     (statreg &= 0xDFFF)
#define CLR_CARRY  (statreg &= 0xEFFF)
#define CLR_OVER   (statreg &= 0xF7FF)
#define CLR_ODDP   (statreg &= 0xFBFF)
#define CLR_XOP    (statreg &= 0xFDFF)
#define CLR_NONPRV (statreg &= 0xFEFF)
#define CLR_MAP    (statreg &= 0xFF7F)
#define CLR_PROT   (statreg &= 0xFFBF)
#define CLR_OVERI  (statreg &= 0xFFDF)
#define CLR_WCS    (statreg &= 0xFFEF)

#define SET_LGT    (statreg |= 0x8000)
#define SET_AGT    (statreg |= 0x4000)
#define SET_EQ     (statreg |= 0x2000)
#define SET_CARRY  (statreg |= 0x1000)
#define SET_OVER   (statreg |= 0x0800)
#define SET_ODDP   (statreg |= 0x0400)
#define SET_XOP    (statreg |= 0x0200)
#define SET_NONPRV (statreg |= 0x0100)
#define SET_MAP    (statreg |= 0x0080)
#define SET_PROT   (statreg |= 0x0040)
#define SET_OVERI  (statreg |= 0x0020)
#define SET_WCS    (statreg |= 0x0010)

#define IS_LGT    (statreg & 0x8000)
#define IS_AGT    (statreg & 0x4000)
#define IS_EQ     (statreg & 0x2000)
#define IS_CARRY  (statreg & 0x1000)
#define IS_OVER   (statreg & 0x0800)
#define IS_ODDP   (statreg & 0x0400)
#define IS_XOP    (statreg & 0x0200)
#define IS_NONPRV (statreg & 0x0100)
#define IS_MAP    (statreg & 0x0080)
#define IS_PROT   (statreg & 0x0040)
#define IS_OVERI  (statreg & 0x0020)
#define IS_WCS    (statreg & 0x0010)

#define SET_MASK(m) (statreg = (statreg & 0xFFF0) | ((m) & 0x000F))
#define GET_MASK    (statreg & 0x000F)

#define GET_MAP     (IS_MAP >> 7)

/*
** Instruction types
*/

#define MAX_INST_TYPES 21
enum optypes
{
   TYPE_1=0,
   TYPE_2,
   TYPE_3,
   TYPE_4,
   TYPE_5,
   TYPE_6,
   TYPE_7,
   TYPE_8,
   TYPE_9,
   TYPE_10,
   TYPE_11,
   TYPE_12,
   TYPE_13,
   TYPE_14,
   TYPE_15,
   TYPE_16,
   TYPE_17,
   TYPE_18,
   TYPE_19,
   TYPE_20,
   TYPE_21,
   TYPE_ILL
};

/*
** Panel actions
*/

enum actions
{
   NULL_ACTION=0,
   QUIT_ACTION,
   STEP_ACTION,
   RUN_ACTION,
   HELP_ACTION,
   LOAD_ACTION,
   PC_ACTION,
   WP_ACTION,
   DISP_ACTION,
   CLEAR_ACTION
};

#define IDLEINST 0x0340

/*
** Object tags
*/

#define BINIDT_TAG	0x01
#define IDT_TAG		'0'
#define ABSENTRY_TAG	'1'
#define RELENTRY_TAG	'2'
#define RELEXTRN_TAG	'3'
#define ABSEXTRN_TAG	'4'
#define RELGLOBAL_TAG	'5'
#define ABSGLOBAL_TAG	'6'
#define CKSUM_TAG	'7'
#define NOCKSUM_TAG	'8'
#define ABSORG_TAG	'9'
#define RELORG_TAG	'A'
#define ABSDATA_TAG	'B'
#define RELDATA_TAG	'C'
#define EOR_TAG		'F'

#define RELSYMBOL_TAG	'G'
#define ABSSYMBOL_TAG	'H'
#define LOAD_TAG	'U'
#define RELSREF_TAG	'V'
#define ABSSREF_TAG	'Y'

#define CHARSPERREC	66  /* Chars per object record */
#define WORDTAGLEN	5   /* Word + Object Tag length */
#define BINWORDTAGLEN	3   /* Binary Word + Object Tag length */
#define EXTRNLEN	11  /* Tag + SYMBOL + addr */
#define GLOBALLEN	11  /* Tag + SYMBOL + addr */
#define IDTTAGLEN	13  /* Tag + IDT + length */
#define SEQUENCENUM	74  /* Where to put the sequence */
#define SYMLEN		6   /* REF/DEF Symbol length */
#define IDTLEN		8   /* IDT length */


/*
** Externals
*/

/* sim990 externals */

extern char *getnum (char *, int *);
extern void panel (void);

/* binloader externals */

extern int loadrec (FILE *, uint8 *, int, int *);
extern int binloader (char *, int);

/* simops externals */

extern void stepprog (void);
extern void runprog (void);
extern uint16 getaddr (uint16, uint16, int);
extern uint8 getbyte (uint16, uint16, int, int);
extern uint16 getword (uint16, uint16, int, int);
extern void putaddr (uint16, uint16, uint16);
extern void putword (uint16, uint16, uint16, int);
extern void cmpbytezero (uint8);
extern void cmpwordzero (uint16);
extern void tracememory (uint32, uint32);
extern uint32 mapaddr (uint16);

extern void cmplongzero (uint32);
extern uint32 getlong (uint16, uint16, int, int);
extern int proc11op (uint16);
extern int proc12op (uint16);
extern int proc13op (uint16);
extern int proc14op (uint16);
extern int proc15op (uint16);
extern int proc16op (uint16);
extern int proc17op (uint16);
extern int proc18op (uint16);
extern int proc19op (uint16);
extern int proc20op (uint16);
extern int proc21op (uint16);

/* simio externals */

extern void setbitone (uint16);
extern void setbitzero (uint16);
extern void testbit (uint16);
extern void loadcru (uint16, uint16);
extern uint16 storecru (uint16);
extern int ttyinput;

/*
** Define things specific to the system environment
*/


#if defined(WIN32) 
#include <winsock.h>

#define CLOSE		closesocket
#define IOCTL(a,b,c,d)	ioctlsocket((a),(b),(unsigned long *)(c))
#define ERRNO		WSAGetLastError()
#define PERROR(m)	fprintf (stderr, "%s: errno = %d\n", (m), ERRNO);
typedef unsigned long	caddr_t;
#ifndef ETIMEDOUT
#define ETIMEDOUT	WSAETIMEDOUT
#endif
#ifndef EADDRINUSE
#define EADDRINUSE	WSAEADDRINUSE
#endif
#ifndef ECONNREFUSED
#define ECONNREFUSED	WSAECONNREFUSED
#endif
#ifndef EINPROGRESS
#define EINPROGRESS	WSAEWOULDBLOCK
#endif
#ifndef ENOTCONN
#define ENOTCONN	WSAENOTCONN
#endif
#ifndef ECONNREFUSED
#define ECONNREFUSED	WSAECONNREFUSED
#endif
#ifndef ENOBUFS
#define ENOBUFS	        WSAENOBUFS
#endif
#endif /* WIN32  */

/*
** Define things left undefined above
*/

#ifndef ERRNO
#define ERRNO		errno
#endif
#ifndef CLOSE
#define CLOSE		close
#endif
#ifndef READ
#define READ		read
#endif
#ifndef WRITE
#define WRITE		write
#endif
#ifndef PERROR
#define PERROR		perror
#endif
#ifndef SELECT
#define SELECT		select
#endif
#ifndef IOCTL
#define IOCTL		ioctl
#endif

