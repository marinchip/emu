## Marinchip Software Stack

This is a brief description of the Marinchip software stack and the early 2014 status of recovered binaries, manuals and source code. Most the manuals are available from the Marinchip pages on the [Fourmilab](https://www.fourmilab.ch/documents/marinchip/) site and from the Documentation page on the [Powertran Cortex](http://www.powertrancortex.com/documentation.html) site.

### 1. Shell utilities

Marinchip developed the usual set of utilities for use with both MDEX and NOS/MT. These included:

| Tool   | MDEX | Equivalent | Purpose |
|--------|------|------------|---------|
| type   | yes  | cat | copy file to the console
| del    | yes  | rm  | delete file
| dir    | yes  | ls  | list directory
| assume | no   | cd  | set default directory
| create | yes  | -   | create a new file
| bcopy  | yes  | cp  | copy file to other file
| tcopy  | yes  | cp  | copy text file (i.e. stop at EOT marker)
| mcopy  | no   | cp  | copy with globbing
| alias  | no   | ln  | create a new file link
| rename | no   | mv  | rename a file

*(In this table, "equivalent" mean the equivalent Unix programs as they existed on 6th edition.)*

The above list is not exhaustive, see the MDEX and NOS/MT user manuals for a full list. Later versions of NOS/MT added additional commands that mimicked Unix: ls, cd and pwd. The 'ls' command provided a multi-column file listing, 'cd' was like 'assume' and 'pwd' printed the current default directory path.

The command "create" is mostly specific to MDEX: this minimalistic OS did not support dynamic file creation and all files had to be pre-created with a fixed maximum size.

No source code survived, only the binaries and system manual.

### 2. Basic development tools

Marinchip initially developed its software stack on a Univac 1108, using cross-development tools. The first programs developed included a native development stack. John recalls:

*"MDEX (which was originally called Sisyphus Junior) was written with the Univac [cross-] assembler, downloaded via the mechanism described above, and tested on the S-100 prototype machine. Debugging was via the PROM monitor in TIOS, which allowed examining and changing registers and memory without the need for a front panel.*

*MDEX, the Assembler, Linker, Editor, and Debugger, were all originally written on the Univac assembler in its syntax. The Assembler, however, used the standard TI syntax for its instructions. When I wrote of MDEX being rewritten as native, I was referring to converting the syntax of the assembly code from that used by the Univac cross-assembler to native TI syntax which my assembler used and moving the development from the Univac to the 9900 machine itself. By some time in 1979, all development had moved to the 9900 and the Univac was no longer used."*

#### (a) Line editor ("EDIT")

The line editor is similar to Unix 'ed', but without the regular expression capabilities. Instead it offers simple search and and replace facilities. It uses overflow files in order to edit files much larger than available memory. Perhaps this is the same "linedb" code that is used in the "window" editor, but this is not certain.

No source code survived, only the binary and manual.

#### (b) Assembler ("ASM")

The assembler is similar to Unix 'as', offering a more or less identical feature set. It does not support seperate segments, nor does it offer temporary labels. In actual use, it feels very similar.

No source code survived, only the binary and manual.

#### (c) Linker / Loader ("LINK")

The link-loader is similar to 'ld' in function. It does not support partial linking (linking several relocatable object files into a combined relocatable object file), but this is not much missed in practice.

Its approach to relocatable object libraries is different. Rather than using a single file ("tar") archive, it uses a directory of relocatable object files and index files, relating symbols to the defining file (similar to the index generated by 'ranlib' on later Unix).

No source code survived, only the binary and manual (a chapter in the OS manual)

### 3. Compilers

Marinchip offered a range of compilers, most of which were developed externally. Marinchip never adopted a "system compiler", although QBasic came close to it for later utilities.

#### (a) Pascal

The Pascal compiler is a straight port of Per Birnch Hansen's compiler written for the experimental "Solo" operating system. It compiles to a word-oriented intermediate code that is interpreted at runtime -- somewhat similar to USCD pascal.

This version of Pascal only allows top level procedures and functions (like C) and implements a "universal" datatype that can be used for casting data types. These changes make it more suitable to system code. However, it seems that Marinchip quickly concluded that it fell short as a systems compiler and the code was not developed beyond the initial port.

Through the Solo distribution tape, all Pascal sources survived and the runtime as a binary. The runtime source code has been reconstructed.

#### (b) META

The META compiler-compiler seems to have been a derivative of the META 3 paper. John Walker handcoded a set of assembler runtime routines for the 9900 and MDEX and developed META from that base. META is a recursive descend parser generator, similar in nature to TMG that existed on early Unix, and later replaced by Yacc.

META was used to write two compilers for the M9900, one for a new language "SPL" and one for Basic.

All original sources for META survived, along with its manual and binaries.

#### (c) SPL

The "System Programming Language" or SPL was not a Marinchip product, but developed by Mike Riddle, doing business as "Evolution Computing". It was however distributed by Marinchip.

SPL as a language is highly reminiscent of PL/M. Beyond PL/M it has a built-in (formatted) I/O package. The subroutine facilities are somewhat strange: all parameters are passed by reference, not by value and subroutine local variables are static (in the C sense). Hence, recusursion has to be used with caution; it is somewhat alleviated by having a stack datatype.

SPL was written using a META based front-end that generates 9900 assembler. There is a second optimizer pass to improve the output code. The compiler generates direct code for integer, byte and address operations, but falls back on library calls for anything else.

SPL was not adopted by Marinchip as its systems language, as initial releases were unstable and the language was still evolving. Also, the code generated was substantially less efficient than that generated by the Unix C compiler.

Only its binaries and manuals survive, no source code has been found to date. A few SPL programs have survived, notably the object file disassembler written by Peter Buro in the mid-1980's and heavily used for recreating Marinchip source code.

#### (d) QBasic

QBasic was a product commisioned and owned by Marinchip, but developed under contract by Mike Riddle, with much input from Dan Drake at Marinchip. The language was compatible with "C-Basic", which was a very popular product on CP/M at the time. Many programs ran without change or with minimal tweaking.

The structure of QBasic seems to have initially been based on that of the SPL compiler: a META front-end generating assembler followed by an optimizer pass. Later editions seem to have changed this a bit, with the first pass generating a custom intermediate code and a second pass generating (indirect) threaded code. This change made programs significantly shorter, with a minimal impact on performance.

QBasic was used to create application pacakges for the M9900 system: a full screen editor, a spell checker and an accounting package.

Only its binaries and manuals survive, no source code has been found to date.

### 4. Interpreters

#### (a) Basic

Obligatory for a late 70's computer system, Marinchip included a Basic interpreter with its system. This basic came in two versions: a standard Basic, which was similar to Microsoft Disk Basic and other Basic's of the era and "extended commercial Basic", which offered higher precision floating point, more support for formatted print and the ability to "chain" programs.

For standard Basic, only its binaries and manuals survive, no source code has been found to date. For extended commercial Basic only the manual survives (standard and extended share a common manual).

#### (b) Script

Mentioned for completeness, NOS/MT did allow writing scripts of regular commands. This was very limited, essentially the command interpreter reading lines from a file instead of from the terminal. It did not support parameters or variables -- in short, it was similar to the .BAT files of early DOS.

The script language was mostly used for install procedures and compile scripts. it existed only on NOS/MT, not on MDEX.

### 5. Word processing

John was a prolific writer, and word processing tools appeared quickly on the M9900. In this sense it has a shared root with Unix, that was initially sold to Bell Labs management as a word processing system.

#### (a) Screen editor("WINDOW")

Window is a full screen editor, somewhat similar to 'vi' on Unix. Like vi it is bimodal, with a screen mode and a command mode. It does not use a terminal capabilities database, but instead is compiled with the appropriate drivers for a particular terminal (probably most M9900 systems only had one or two terminals). It only offers basic search and replace, i.e. not the regular expression based versions found in 'ed' and 'vi'.

Window can run in as little as 48KB of memory and can comfortably edit large files through the clever use of overflow files. It is surprisingly usable, exceeding 'vi' as it stood in 1980.

Window was written in QBasic, along with a few support routines in assembler. The overflow file code was maybe re-used from the line editor described above.

Window was sold as the first AutoDesk product under the name "AutoScreen". It was later recoded in C and in every day use at AutoCAD during the mid-80's.

The full QBasic source code for Window survived as did the manual, and the source code for the assembler assists has been recreated.

#### (b) Formatter ("WORD")

Word was among the first programs created for the M9900 and is a text formatter in the style of RUNOFF. Hence, it is similar to 'roff' and 'nroff' on Unix, which are also patterned after RUNOFF.

Like the Unix equivalents, Word was written in assembler. It was heavily used to prepare all the manuals for the Marinchip software. So far, not many source files for these Marinchip manuals have been found (just META actually) - what survived were paper copies.

As to word itself, only its binaries and manuals survive, no source code has been found to date.

#### (c) Spell checker ("SPELL")

Spell was a late addition to the Marinchip software stack. Its function is the same as its Unix namesake.

According to a contemporary review of the M9900 system and software by Duff Kirkland, Spell was written in QBasic. It is undocumented what algorithms it uses to make the spell check quick, but probably John Walker put some thought into that.

Only its binaries, dictonary and manuals survive, no source code has been found to date.

### 6. Applications

Beyond the programmer oriented software stack above, Marinchip marketed two application packages: an accounting package and a computer-aided design (drafting) package.

#### (a) Osborne Accounting

In the late 1970's, Osborne published a set of 3 books by Lon Poole and Mary Borchers that explained how to create a computerised accouting system. The books included source code in CBasic. These books are still available from book antiquariats and from archive.org:

  *  [General Ledger](https://archive.org/details/General_Ledger_1979_Adam_Osborne)

  *  [Accounts Receivable & Payable](https://archive.org/details/accountspayablea0000pool)

  *  [Payroll with cost accounting]

Marinchip converted the system from these books to work on the M9900, and maybe QBasic was in part created to enable this. The package is a nice historic demonstration of the business software being created with CBasic / QBasic around 1980. By 1983 this niche had mostly shifted to using dBase II. Although Marinchip contemplated doing its own conversion of dBase ancestor "JPLDIS", this never happened as the focus shifted to AutoCAD (see below).

The CBasic source code for this accounting package survived in various CP/M software archives. As pointed out in the QBasic manual, CBasic source code requires some tweaks in order to compile (such as a prefix zero versus a postfix H for hexadecimal numbers). With such tweaks, it works fine.

The package as adapted by Marinchip itself has so far not been found, not in binary and not in source. However, a new adaptation of the original CBasic version of the General Ledger module has been created.

#### (b) Interact

The most intriguing part of the M9900 software stack is Interact. Interact was a computer aided drafting package, written by Mike Riddle and distributed by Marinchip. It sold in limited numbers. Note that the required hardware (a M9900 system, a digitizer tablet and a hi-res (for the time) monitor board would have been quite costly. However, it would still be much lower cost than the mini-computer based systems that it was competing with.

John writes in the "before AutoCAD" paper: *"[On New Year's Day 1980], I was celebrating not just the dawn of the 1980s, which I hoped would be an improvement over the Souring Seventies, but also the first successful multi-user test, achieved minutes before midnight, of NOS/MT, the Unix-like operating system that seemed to be the future of Marinchip, my company at the time. What was to eventually become AutoCAD was, on that night, less than 2000 lines of undocumented code in an obscure language for an unpopular computer.”*

That last line refers to INTERACT, and apparently it was 2,000 lines of SPL code for the M9900 at that time. Presumably, there are some assembler assist modules as well. Wouldn't it be cool to resurrect that version of Interact?

John explained: *"There is a printed listing of the Interact source code in the store-all. It is in SPL, and you'd have to find a runnable SPL compiler or port it to another language. That's what the first versions of AutoCAD were: ports of the SPL code to PL/I (for the 8080) and C (for the 8086). The first version of AutoCAD we shipped to customers in 1982 was far extended beyond this initial port of Interact."*

There is some more background on Interact here:
[https://www.3dcadworld.com/autocads-ancestor/](https://www.3dcadworld.com/autocads-ancestor/)

Of course, today we have M9900 emulators, working MDEX and NOS/MT operating systems and a working SPL compiler from early 1981. Resurrecting this AutoCAD ancestor seems perfectly doable.

### 7. Reflection

All in all it is amazing how much stuff Marinchip was able to pull together in just a few years. The entire journey from devising the 9900 board for the S-100 bus to having a complete system with a complete software stack was completed in just 3 years or so, moonshining in the evening hours and weekends. It took Unix 6 years full-time to achieve about the same -- but Marinchip obviously had the advantage of starting almost a decade later, and with a good understanding of the Unix design and leveraging public domain software where it could.

The one thing that Marinchip missed was a compiler that could translate high level system code into efficient assembler code and was small enough to run natively. Had NOS/MT been written in a portable systems language, it might have survived much longer than it did and maybe even attracted IBM's attention:

[https://spectrum.ieee.org/the-inside-story-of-texas-instruments-biggest-blunder-the-tms9900-microprocessor](https://spectrum.ieee.org/the-inside-story-of-texas-instruments-biggest-blunder-the-tms9900-microprocessor)

